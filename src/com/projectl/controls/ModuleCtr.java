package com.projectl.controls;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.projectl.beans.Etudiant;
import com.projectl.beans.Module;
import com.projectl.dao.DAOException;
import com.projectl.dao.EtudiantDAO;
import com.projectl.dao.ModuleDAO;



public class ModuleCtr {

	private ModuleDAO daoMod ;
	private String user ;
	private HttpServletRequest request ;
	private Module module ;
	private ArrayList<String> erreurs = new ArrayList<String>();

	public static final Path CHEMIN_IMAGES = Paths.get(System.getProperty("user.home"),"uploads") ;

	public ModuleCtr(String user){
		this.user = user ;
	}

	public ModuleCtr(ModuleDAO daoMod, String user, HttpServletRequest request){
		this.request = request ;
		this.daoMod = daoMod ;
		this.user = user;
	}

	public int creerModule(){
		module = new Module(); 
		int id = -1 ;	
		verifierNom();
		verifierDescription();
		verifierImage();
		if(erreurs.size()==0){
			module.setNiveau(request.getParameter("niveau"));
			module.setEstVisible(true);
			module.setDateParution(new java.util.Date());
			module.setResponsable(user);
			id = daoMod.create(module);
		}
		request.setAttribute("erreurs", erreurs);
		return id;
	}
	
	public void supprimerModule(Integer clefModule){
		// TODO : A implémenter après suppression cours (détruire les liens étudiant-module et les cours)
	}

	public void inscrireEtudiant(EtudiantDAO etudiantDAO, Integer clefModule) throws DAOException{
		Etudiant etudiant = etudiantDAO.read(user);
		if(etudiant==null){
			etudiant = new Etudiant();
			etudiant.setEmail(user);
			etudiant.setNiveau("1");
			etudiantDAO.create(etudiant);
		}
		if(!etudiantDAO.isEtudiant(user,clefModule)){
			etudiantDAO.linkToModule(user,clefModule);
		}			
	}

	public void desinscrireEtudiant(EtudiantDAO etudiantDAO, Integer clefModule) throws DAOException{
		if(etudiantDAO.isEtudiant(user,clefModule)){
			etudiantDAO.unlinkFromModule(user, clefModule);
			if(!etudiantDAO.isEtudiant(user))
				etudiantDAO.delete(user);
		}
			
	}

	private void verifierNom(){
		if(request.getParameter("nom").equals(""))
			erreurs.add("Vous devez spécifier un nom pour le module");
		else{
			module.setNom(request.getParameter("nom"));
		}

	}

	private void verifierDescription(){
		if(request.getParameter("description").equals(""))
			erreurs.add("Vous devez ajouter une description au module.");
		else
			module.setDescription(request.getParameter("description"));
	}

	private void verifierImage(){
		try{
			Part part = request.getPart("image"); //récupération de l'image
			InputStream image = part.getInputStream();
			BufferedImage img = ImageIO.read(image); //vérification s'il s'agit d'une image
			if(img==null){
				erreurs.add("Veuillez entrer une image.");
			}else{
				Path fichier = Files.createTempFile(CHEMIN_IMAGES, "module-", ".png");
				ImageIO.write(img, "png", fichier.toFile());
				module.setImage(fichier.getFileName().toString());
			}
		}catch(IOException e){
			erreurs.add("L'extension de ce fichier n'est pas supportée.");
		}catch(ServletException e){
			erreurs.add("Une erreur est survenue, veuillez réessayer.");
		}
	}

}