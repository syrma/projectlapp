package com.projectl.controls;

import javax.servlet.http.HttpServletRequest;

import com.projectl.beans.Cours;
import com.projectl.dao.CoursDAO;
import com.projectl.dao.DAOException;

public class CoursCtr {

	private static final String PARAM_NOM_COURS = "nom" ;
	private static final String ATTR_ERREUR = "erreur" ;

	private CoursDAO daoC ; 
	
	public CoursCtr(CoursDAO daoC){
		this.daoC = daoC ;
	}
	
	public int creerCours(Integer idModule,Integer indice,HttpServletRequest request) throws DAOException{
		int i = -1;
		Cours cours = new Cours();
		cours.setClefModule(idModule);
		cours.setIndice(indice);
		String nomCours = request.getParameter(PARAM_NOM_COURS);
		if(nomCours != null && !nomCours.equals("")){
			cours.setNom(nomCours);
			i = daoC.create(cours);
			return i;
		}
		request.setAttribute(ATTR_ERREUR, "Veuillez remplir le nom du cours.");
		return i;
	}
	
	public void modifierFaq(String faq,Integer clefCours) throws DAOException{
		
		daoC.updateFAQ(clefCours,faq);
		
		
	}
}
