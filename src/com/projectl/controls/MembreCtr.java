package com.projectl.controls;

import java.text.SimpleDateFormat;
import java.util.ArrayList ;

import com.projectl.dao.MembreDAO;
import com.projectl.dao.DAOException;
import com.projectl.beans.Membre;

import javax.servlet.http.HttpServletRequest ;
import javax.servlet.http.HttpSession;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

public class MembreCtr {
	
	private static final String PARAMETRE_NOM = "nom";
	private static final String PARAMETRE_PNOM = "pnom";
	private static final String PARAMETRE_EMAIL = "email";
	private static final String PARAMETRE_MDPASSE = "password";
	private static final String PARAMETRE_CONF_MDPASSE = "confpassword";
	private static final String PARAMETRE_MDPASSE_MODIF = "oldpassword";
	private static final String PARAMETRE_SEXE = "sexe";

	private static final String ALGO_CHIFFREMENT = "SHA-256";
	
	private HttpServletRequest requete ;
	private MembreDAO dao ;
	private ArrayList<String> erreurs = new ArrayList<String>();
	private String resultat ;
	private Membre membre ;
	
	public ArrayList<String> getErreurs() {
		return erreurs;
	}
	public String getResultat() {
		return resultat;
	}
	
	public Membre getMembre() {
		return membre;
	}
	
	public MembreCtr(MembreDAO dao, HttpServletRequest requete){
		this.dao = dao ;
		this.requete = requete ;
		this.membre = new Membre();
	}
	
	public void inscrire(){
		verifierEmail();
		verifierMdpasse();
		verifierIdentite();
		
		if(erreurs.size()==0){
			try{
				membre.setEstModerateur(false);
				membre.setDateInscription(new java.util.Date());
				dao.create(membre);
				resultat = "Inscription réussie.";
			}catch(DAOException e){
				e.printStackTrace();
				resultat = "L'inscription n'a pas pu se terminer correctement, merci de réessayer.";
			}
		}
		else{
			resultat = "Échec de l'inscription.";
		}
		requete.setAttribute("listeErreurs", erreurs);
		requete.setAttribute("resultat", resultat);
		
	}
	
	public void modifier(){
		String email = (String) requete.getSession().getAttribute("email");
		membre = dao.read(email);
		verifierOldPass();
		modifierIdentite();
		
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		try{
			membre.setDateNaissance(sd.parse(requete.getParameter("dateNaissance")));
		}catch(Exception e){}
					
		if(!requete.getParameter(PARAMETRE_MDPASSE).equals(""))
			verifierMdpasse();
		
		if(!requete.getParameter(PARAMETRE_SEXE).equals(""))
			membre.setSexe(requete.getParameter(PARAMETRE_SEXE).charAt(0));
		

		if(erreurs.size()==0){
			try{
				dao.update(email,membre);
				resultat = "Modification réussie.";
			}catch(DAOException e){
				e.printStackTrace();
				resultat = "Une erreur est survenue lors de la modification, merci de réessayer.";
			}
		}
		else{
			resultat = "Échec de la modification.";
		}
		requete.setAttribute("listeErreurs", erreurs);
		requete.setAttribute("resultat", resultat);
		
	}

	private void verifierOldPass(){
		String mdp = requete.getParameter(PARAMETRE_MDPASSE_MODIF);

		ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
		passwordEncryptor.setAlgorithm(ALGO_CHIFFREMENT);
		passwordEncryptor.setPlainDigest(false);	
		if(!passwordEncryptor.checkPassword(mdp, membre.getMdpasse()))
			erreurs.add("L'ancien mot de passe est erroné.");

	}
	
	private void verifierEmail(){
		String mail = requete.getParameter(PARAMETRE_EMAIL);
		if(dao.read(mail)==null){
			if(mail!=null && mail.trim().length()!=0)
				membre.setEmail(mail);
			else
				erreurs.add("L'e-mail est invalide.");
		}
		else
			erreurs.add("Cette adresse existe déjà, veuillez vous authentifier.");		
	}

	
	
	private void verifierMdpasse(){
		String mdp = requete.getParameter(PARAMETRE_MDPASSE);
		String mdpConf= requete.getParameter(PARAMETRE_CONF_MDPASSE);
		
		if(mdp== null || mdpConf==null){
			erreurs.add("Il faut remplir les deux champs de mot de passe.");
			return;
		}
		if(mdp.length() < 4)
			erreurs.add("Le mot de passe doit être constitué d'au moins 4 charactères.");
		else if(!mdp.equals(mdpConf))
			erreurs.add("Le mot de passe et la confirmation ne sont pas identiques.");
		else{
			ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
			passwordEncryptor.setAlgorithm(ALGO_CHIFFREMENT);
			passwordEncryptor.setPlainDigest(false);
			String mdpChiffre = passwordEncryptor.encryptPassword(mdp);
			membre.setMdpasse(mdpChiffre); 
		}
	}
	private void verifierIdentite(){
		String nom = requete.getParameter(PARAMETRE_NOM);
		String pnom = requete.getParameter(PARAMETRE_PNOM);
		if(nom==null || nom.trim().length()==0)
			erreurs.add("Veuillez entrer un nom.");
		membre.setNom(nom);
		if(pnom==null || pnom.trim().length()==0)
			erreurs.add("Veuillez entrer un prénom.");
		membre.setPrenom(pnom);
	}
	private void modifierIdentite(){
		String nom = requete.getParameter(PARAMETRE_NOM);
		String pnom = requete.getParameter(PARAMETRE_PNOM);
		System.out.println(pnom);
		if(nom!=null && nom.trim().length()!=0)
			membre.setNom(nom);
		if(pnom!=null && pnom.trim().length()!=0)
			membre.setPrenom(pnom);
	}
	
}
