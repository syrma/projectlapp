package com.projectl.beans;

import java.io.Serializable;

public class Lecon extends Seance implements Serializable{

	private static final long serialVersionUID = 1L;

	String urlFichier,extension ;

	public String getUrlFichier() {
		return urlFichier;
	}
	public void setUrlFichier(String urlFichier) {
		this.urlFichier = urlFichier;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}

	
}
