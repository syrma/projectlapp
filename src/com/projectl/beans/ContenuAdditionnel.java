package com.projectl.beans;

import java.io.Serializable;

public class ContenuAdditionnel implements Serializable{

	private static final long serialVersionUID = 1L;

	private int clefCA ; 
	private int Seance ; 
	private String nom ;
	private String urlFichier ;
	private String extension ;
	
	
	public int getClefCA() {
		return clefCA;
	}
	public void setClefCA(int clefCA) {
		this.clefCA = clefCA;
	}
	public int getSeance() {
		return Seance;
	}
	public void setSeance(int seance) {
		Seance = seance;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getUrlFichier() {
		return urlFichier;
	}
	public void setUrlFichier(String urlFichier) {
		this.urlFichier = urlFichier;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	
}
