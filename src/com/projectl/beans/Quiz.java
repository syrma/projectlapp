package com.projectl.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class Quiz extends Seance implements Serializable{

	private static final long serialVersionUID = 1L;
	private ArrayList<Question> questions ;
	
	public ArrayList<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	} 
	
}
