package com.projectl.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class Question implements Serializable{

	private static final long serialVersionUID = 1L;
	private ArrayList<Reponse> reponses; 
	Integer clefQuestion;
	String contenu;
	public Integer getClefQuestion() {
		return clefQuestion;
	}
	public void setClefQuestion(Integer clefQuestion) {
		this.clefQuestion = clefQuestion;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public ArrayList<Reponse> getReponses() {
		return reponses;
	}
	public void setReponses(ArrayList<Reponse> reponses) {
		this.reponses = reponses;
	}
	
	
}
