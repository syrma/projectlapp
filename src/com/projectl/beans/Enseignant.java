package com.projectl.beans;

import java.io.Serializable;

public class Enseignant extends Membre implements Serializable{
	private static final long serialVersionUID = 1L;

	private String email ;
	private Boolean estResponsable;

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public Boolean getEstResponsable() {
	return estResponsable;
}

public void setEstResponsable(Boolean estResponsable) {
	this.estResponsable = estResponsable;
}


}
