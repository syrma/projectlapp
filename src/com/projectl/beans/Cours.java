package com.projectl.beans;

import java.io.Serializable;

public class Cours implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer clefCours,clefModule,indice ;
	private String nom ;
	private String faq ;
	public Integer getClefCours() {
		return clefCours;
	}
	public void setClefCours(Integer clefCours) {
		this.clefCours = clefCours;
	}
	public Integer getIndice() {
		return indice;
	}
	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	public Integer getClefModule() {
		return clefModule;
	}
	public void setClefModule(Integer clefModule) {
		this.clefModule = clefModule;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getFaq() {
		return faq;
	}
	public void setFaq(String faq) {
		this.faq = faq;
	}
	
	

}
