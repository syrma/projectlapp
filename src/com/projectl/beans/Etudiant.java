package com.projectl.beans;

import java.io.Serializable;

public class Etudiant extends Membre implements Serializable {

	private static final long serialVersionUID = 1L;

	private String email, niveau ;

	private Integer note; //Cette note n'appartient pas à l'entité est n'est là que pour l'affichage dans un cas précis dans un module précis.
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	public Integer getNote() {
		return note;
	}

	public void setNote(Integer note) {
		this.note = note;
	}
	
	
}
