package com.projectl.beans;

import java.io.Serializable;
import java.util.Date;

public class Membre implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String nom,prenom,mdpasse,email, avatar;
	private Boolean estModerateur ;
	private Character sexe ;
	private Date dateNaissance,dateInscription ;
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getMdpasse() {
		return mdpasse;
	}
	public void setMdpasse(String mdpasse) {
		this.mdpasse = mdpasse;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getEstModerateur() {
		return estModerateur;
	}
	public void setEstModerateur(Boolean estModerateur) {
		this.estModerateur = estModerateur;
	}
	public Character getSexe() {
		return sexe;
	}
	public void setSexe(Character sexe) {
		this.sexe = sexe;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public Date getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}
	
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public String getAvatarURL(){
		if(avatar==null)
			return "img/150x150";
		else
			return "image/" + avatar;
	}
	
	public String getNomComplet(){
		if(nom==null)
			return "Pas encore assigné";
		else
			return prenom + " " + nom ;
	}
	@Override
	public String toString() {
		return "Membre [nom=" + nom + ", prenom=" + prenom + ", mdpasse="
				+ mdpasse + ", email=" + email + ", avatar=" + avatar
				+ ", estModerateur=" + estModerateur + ", sexe=" + sexe
				+ ", dateNaissance=" + dateNaissance + ", dateInscription="
				+ dateInscription + "]";
	}
	
}
