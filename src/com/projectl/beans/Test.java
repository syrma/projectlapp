package com.projectl.beans;

import java.io.Serializable;
import java.util.ArrayList;

public class Test implements Serializable{
	private static final long serialVersionUID = 1L;

	private Integer clefTest;
	private ArrayList<Question> questions ;
	
	
	public ArrayList<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}
	public Integer getClefTest() {
		return clefTest;
	}
	public void setClefTest(Integer clefTest) {
		this.clefTest = clefTest;
	} 
	
}
