package com.projectl.beans;

import java.io.Serializable;

public abstract class Seance implements Serializable{

	private static final long serialVersionUID = 1L;
	
	Integer clefSeance,clefCours,indice;
	String nom,typeSeance;
	
	public Integer getClefSeance() {
		return clefSeance;
	}
	public void setClefSeance(Integer clefSeance) {
		this.clefSeance = clefSeance;
	}
	public Integer getClefCours() {
		return clefCours;
	}
	public void setClefCours(Integer clefCours) {
		this.clefCours = clefCours;
	}
	public Integer getIndice() {
		return indice;
	}
	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getTypeSeance() {
		return typeSeance;
	}
	public void setTypeSeance(String typeSeance) {
		this.typeSeance = typeSeance;
	}

}
