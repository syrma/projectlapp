package com.projectl.beans;

import java.io.Serializable;
import java.util.Date;

public class Module implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer clefModule,testFinal ;
	private String nom, description,niveau,image ;
	private Boolean estVisible ;
	private Date dateParution ;
	private String responsable,auteur,tuteur ;
	public Integer getClefModule() {
		return clefModule;
	}
	public void setClefModule(Integer clefModule) {
		this.clefModule = clefModule;
	}
	public Integer getTestFinal() {
		return testFinal;
	}
	public void setTestFinal(Integer testFinal) {
		this.testFinal = testFinal;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNiveau() {
		return niveau;
	}
	public String getNiveauLisible(){
		switch(niveau){
		case "1" : return "Basique" ;
		case "2" : return "Intermédiaire" ;
		case "3" : return "Avancé";
		default : return null ;
		}
	}
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	public Boolean getEstVisible() {
		return estVisible;
	}
	public void setEstVisible(Boolean estVisible) {
		this.estVisible = estVisible;
	}
	public Date getDateParution() {
		return dateParution;
	}
	public void setDateParution(Date dateParution) {
		this.dateParution = dateParution;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getTuteur() {
		return tuteur;
	}
	public void setTuteur(String tuteur) {
		this.tuteur = tuteur;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getImageURL(){
		if(image==null)
			return "img/230x230" ;
		else
			return "image/" + image ;
	}
	
	
}
