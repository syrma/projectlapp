package com.projectl.beans;

import java.io.Serializable;

public class Reponse implements Serializable{

	private static final long serialVersionUID = 1L;

	Integer clefReponse,clefQuestion; 
	String contenu;
	Boolean estCorrecte;
	
	public Integer getClefReponse() {
		return clefReponse;
	}
	public void setClefReponse(Integer clefReponse) {
		this.clefReponse = clefReponse;
	}
	public Integer getClefQuestion() {
		return clefQuestion;
	}
	public void setClefQuestion(Integer clefQuestion) {
		this.clefQuestion = clefQuestion;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public Boolean getEstCorrecte() {
		return estCorrecte;
	}
	public void setEstCorrecte(Boolean estCorrecte) {
		this.estCorrecte = estCorrecte;
	}
	
}
