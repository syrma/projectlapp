package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.ContenuAdditionnel;

public interface ContenuAdditionnelDAO {
	int create(ContenuAdditionnel ca) throws DAOException, IllegalArgumentException;
	ContenuAdditionnel read(Integer clefCA) throws DAOException ;
	void update(Integer clefCA,ContenuAdditionnel ca) throws DAOException, IllegalArgumentException ;
	void delete(Integer clefCA) throws DAOException, IllegalArgumentException ;
	ArrayList<ContenuAdditionnel> list(Integer clefSeance) throws DAOException ;
}
