package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.ContenuAdditionnel;

public class ContenuAdditionnelDAOImpl implements ContenuAdditionnelDAO {
	static final String SQL_CREATE = "INSERT INTO ContenuAdditionnel(clefSeance,nom,urlFichier,extension) values(?,?,?,?);";
	static final String SQL_READ = "SELECT * FROM ContenuAdditionnel WHERE clefCA=?;";
	static final String SQL_LIST = "SELECT * FROM ContenuAdditionnel WHERE clefSeance = ? ;";

	static final String SQL_UPDATE = "UPDATE ContenuAditionnel SET clefSeance = ?, nom = ?, urlFichier = ?, extension = ? WHERE clefCA = ?;";
	static final String SQL_DELETE = "DELETE FROM ContenuAdditionnel WHERE clefCA=?;";
	DAOFactory factory ; 
	
	

	public ContenuAdditionnelDAOImpl(DAOFactory factory) {
		this.factory = factory;
	}

	
	@Override
	public int create(ContenuAdditionnel ca) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, true, ca.getSeance(), ca.getNom(), ca.getUrlFichier(), ca.getExtension());
			stmt.executeUpdate();
			ResultSet rs  = stmt.getGeneratedKeys();
			if(rs.next())
				return rs.getInt(1);
			else
				throw new DAOException("Échec de l'ajout du contenu.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}

	@Override
	public ContenuAdditionnel read(Integer clefCA) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefCA);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void update(Integer clefCA, ContenuAdditionnel ca)
			throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,ca.getSeance(), ca.getNom(), ca.getUrlFichier(), ca.getExtension(), clefCA);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification de ce contenu, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public void delete(Integer clefCA) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefCA);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de ce contenu.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public ArrayList<ContenuAdditionnel> list(Integer clefSeance) throws DAOException {
		ArrayList<ContenuAdditionnel> liste = new ArrayList<ContenuAdditionnel>();
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_LIST, false, clefSeance);
			rs = stmt.executeQuery();
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}
		
	
	private ContenuAdditionnel map(ResultSet rs) throws SQLException{
		ContenuAdditionnel ca = new ContenuAdditionnel();
		ca.setClefCA(rs.getInt("clefCA"));
		ca.setSeance(rs.getInt("clefSeance"));
		ca.setNom(rs.getString("nom"));
		ca.setUrlFichier(rs.getString("urlFichier"));
		ca.setExtension(rs.getString("extension"));
		return ca;
	}

}
