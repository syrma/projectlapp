package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Enseignant;
import com.projectl.beans.Lecon;

public class EnseignantDAOImpl implements EnseignantDAO {

	static final String SQL_CREATE = "INSERT INTO Enseignant values(?,?);";
	static final String SQL_READ = "SELECT * FROM Enseignant WHERE email=?;";
	static final String SQL_LIST = "SELECT * FROM Enseignant;";
	static final String SQL_READ_RESPONSABLE = "SELECT * FROM Enseignant WHERE estResponsable = 1 AND email=? ;";
	static final String SQL_UPDATE = "UPDATE Enseignant SET estResponsable = ? WHERE clefEnseignant = ? ";
	static final String SQL_DELETE = "DELETE FROM Enseignant WHERE clefEnseignant = ?; " ;
	DAOFactory factory ;
	
	public EnseignantDAOImpl(DAOFactory factory){
		this.factory = factory ; 
	}
	
	@Override
	public void create(Enseignant enseignant) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, false, enseignant.getEmail(), enseignant.getEstResponsable());
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de l'ajout de l'enseignant.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Enseignant read(String email) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, email);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}
	@Override
	public Enseignant readResponsables(String email) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ_RESPONSABLE, false, email);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void update(String email, Enseignant responsable)
			throws DAOException, IllegalArgumentException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(String email) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, email);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de l'enseignant.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}
	
	public Enseignant map(ResultSet rs) throws SQLException{
		Enseignant enseignant = new Enseignant();
		enseignant.setEmail(rs.getString("email"));
		enseignant.setEstResponsable(rs.getBoolean("estResponsable"));
		return enseignant ;
	}

	@Override
	public ArrayList<Enseignant> list() throws DAOException {
		ArrayList<Enseignant> liste = new ArrayList<Enseignant>();
		Connection cnx = null ;
		Statement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_LIST);
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

}
