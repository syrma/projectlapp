package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.projectl.beans.Question;
import com.projectl.beans.Reponse;
import com.projectl.beans.Test;

public class TestDAOImpl implements TestDAO {

	static final String SQL_CREATE = "INSERT INTO TestFinal VALUES();";
	static final String SQL_LINK_TEST = "UPDATE Modules SET testFinal = ? WHERE clefModule = ?;";
	static final String SQL_CREATE_QUESTION = "INSERT INTO Question(contenu) VALUES(?);";
	static final String SQL_LINK_QUESTION = "INSERT INTO QuestionTest(clefTest,clefQuestion) VALUES(?,?);";
	static final String SQL_CREATE_REPONSE = "INSERT INTO Reponse(clefQuestion,contenu,estCorrecte) VALUES(?,?,?);";
	
	static final String SQL_READ_TEST ="SELECT Question.clefQuestion, Question.contenu AS contenuQuestion, clefReponse, Reponse.contenu AS contenuReponse, Reponse.estCorrecte FROM"
			+ " QuestionTest INNER JOIN TestFinal ON TestFinal.clefTest = QuestionTest.clefTest INNER JOIN Question ON QuestionTest.clefQuestion = Question.clefQuestion "
			+ "INNER JOIN Reponse ON Question.clefQuestion = Reponse.clefQuestion WHERE TestFinal.clefTest = ?;";

	
	

	static final String SQL_READ_REPONSE = "INSERT INTO Reponse(clefQuestion,contenu,estCorrecte) VALUES(?,?,?);";
	
	static final String SQL_DELETE = "DELETE FROM TestFinal WHERE clefTest=?;";
	private DAOFactory factory ;
	
	public TestDAOImpl(DAOFactory factory){
		this.factory = factory ;
	}
	@Override
	public void create(Test test, Integer clefModule) throws DAOException, IllegalArgumentException {
		
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ; 
		try{
			cnx = factory.getConnection();
			cnx.setAutoCommit(false);
			stmt = initialisationRequetePreparee(cnx,SQL_CREATE, true);
			stmt.executeUpdate();
			rs  = stmt.getGeneratedKeys();
			if(!rs.next())
				throw new DAOException("Échec de l'ajout du test.");
			else{
				Integer clefTest = rs.getInt(1);
				rs.close();
				stmt.close();
				stmt=initialisationRequetePreparee(cnx, SQL_LINK_TEST, false, clefTest, clefModule);
				int module = stmt.executeUpdate();
				stmt.close();
				if(module == 0){
					throw new DAOException("Échec de l'ajout du test au module (identifiant invalide du module)");
				}
				ArrayList<Question> questions = test.getQuestions();
				ArrayList<Reponse> reponses ;
				Integer clefQuestion = null ; 
				for(Question question : questions){
					stmt.close();
					stmt= initialisationRequetePreparee(cnx, SQL_CREATE_QUESTION, true,question.getContenu());
					stmt.executeUpdate();
					rs = stmt.getGeneratedKeys();
					if(!rs.next())
						throw new DAOException("Échec de l'ajout d'une question.");
					else{
						clefQuestion = rs.getInt(1);
						rs.close();
						stmt.close();
						stmt = initialisationRequetePreparee(cnx, SQL_LINK_QUESTION, false, clefTest, clefQuestion);
						stmt.executeUpdate();
						reponses = question.getReponses();
						for(Reponse reponse : reponses){
							stmt.close();
							stmt = initialisationRequetePreparee(cnx, SQL_CREATE_REPONSE, false, clefQuestion, reponse.getContenu(), reponse.getEstCorrecte());
							int i = stmt.executeUpdate();
							if(i ==0)
								throw new DAOException("Échec de l'ajout d'une réponse.");
						}
						
						
					}
						
				
			}
				cnx.commit();
		}}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Test read(Integer clefTest) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ_TEST, false, clefTest);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else{
				Test test= map(rs);
				test.setClefTest(clefTest);
				return test;
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void delete(Integer clefTest) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefTest);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de la séance.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}


	private Test map(ResultSet rs) throws SQLException{
		Test test = new Test();
		ArrayList<Question> questions = new ArrayList<Question>();
		Question question = new Question();
		question.setClefQuestion(rs.getInt("clefQuestion"));
		question.setContenu(rs.getString("contenuQuestion"));
		questions.add(question);
		
		ArrayList<Reponse> reponses = new ArrayList<Reponse>();
		Reponse reponse ;
		rs.beforeFirst();
		while(rs.next()){
			if(rs.getInt("clefQuestion")!=question.getClefQuestion()){	
				question.setReponses(reponses);
				reponses = new ArrayList<Reponse>();
				question = new Question();
				question.setClefQuestion(rs.getInt("clefQuestion"));
				question.setContenu(rs.getString("contenuQuestion"));
				questions.add(question);				
			}
			reponse = new Reponse();
			reponse.setClefReponse(rs.getInt("clefReponse"));
			reponse.setClefQuestion(question.getClefQuestion());
			reponse.setContenu(rs.getString("contenuReponse"));
			reponse.setEstCorrecte(rs.getBoolean("estCorrecte"));
			reponses.add(reponse);
		}
		question.setReponses(reponses);
		test.setQuestions(questions);
	
		
		
		return test;
	}
}
