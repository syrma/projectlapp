package com.projectl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.projectl.beans.Module;

import static com.projectl.dao.DAOUtilitaire.*;

public class ModuleDAOImpl implements ModuleDAO {

	static final String SQL_CREATE = "INSERT INTO Modules(nom,description,niveau,estVisible,dateParution,responsable,auteur,tuteur,testFinal,urlImage) values(?,?,?,?,?,?,?,?,?,?);";
	static final String SQL_READ = "SELECT * FROM Modules WHERE clefModule=?;";
	static final String SQL_LIST = "SELECT * FROM Modules ;";
	static final String SQL_VISIBLE_LIST = "SELECT * FROM Modules WHERE estVisible = 1;";
	static final String SQL_INDEX = "SELECT clefModule, Modules.nom,description,niveau,responsable.nom,"
			+ " responsable.prenom, auteur.nom, auteur.prenom, tuteur.nom, tuteur.prenom FROM Modules "
			+ "LEFT JOIN Membre responsable ON Modules.responsable=responsable.email "
			+ "LEFT JOIN Membre auteur ON Modules.auteur = auteur.email "
			+ "LEFT JOIN Membre tuteur ON Modules.tuteur = tuteur.email ;";
	static final String SQL_UPDATE = "UPDATE Modules SET nom = ?, description = ?, niveau = ?, estVisible = ?,"
			+ "dateParution = ?, responsable = ?, auteur = ? , tuteur = ?, testFinal = ?, urlImage = ?,"
			+ " WHERE clefModule='?';";
	static final String SQL_SET_AUTEUR = "UPDATE Modules SET auteur= ? WHERE clefModule = ? ;";
	static final String SQL_SET_TUTEUR = "UPDATE Modules SET tuteur= ? WHERE clefModule = ? ;";
	static final String SQL_DELETE = "DELETE FROM Modules WHERE clefModule=?;";
	DAOFactory factory ;

	public ModuleDAOImpl(DAOFactory factory) {
		this.factory = factory;
	}

	@Override
	public ArrayList<String[]> index() throws DAOException {
		ArrayList<String[]> index = new ArrayList<String[]>();
		try(Connection cnx = factory.getConnection() ;
			Statement stmt = cnx.createStatement();
			ResultSet rs = stmt.executeQuery(SQL_INDEX)){
			while(rs.next()){
				String[] arr = new String[10];
				for (int i = 0 ; i < 10 ; i++ ){
					arr[i]=rs.getString(i+1);
				}
				index.add(arr);
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
		
		return index;
	}

	@Override
	public int create(Module module) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, true, module.getNom(), module.getDescription(), module.getNiveau(), module.getEstVisible(), module.getDateParution(),module.getResponsable(),module.getAuteur(), module.getTuteur(), module.getTestFinal(), module.getImage());
			stmt.executeUpdate();
			ResultSet rs  = stmt.getGeneratedKeys();
			if(rs.next())
				return rs.getInt(1);
			else
				throw new DAOException("Échec de l'ajout du nouveau module.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public Module read(Integer clefModule) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefModule);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
		
	}
	
	/*"UPDATE Modules SET nom = ?, description = ?, niveau = ?, estVisible = ?,"
	+ "dateParution = ?, responsable = ?, auteur = ? , tuteur = ?, testFinal = ?, urlImage = ?,"
	+ " WHERE clefModule='?';";
*/
	@Override
	public void update(Integer clefModule, Module module) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,module.getNom(),module.getDescription(),module.getNiveau(),module.getEstVisible(),module.getDateParution(),module.getResponsable(), module.getAuteur(), module.getTuteur(), module.getTestFinal(),module.getImage(),clefModule);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du message, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}
	
	@Override
	public void setAuteur(Integer clefModule, String email)
			throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_SET_AUTEUR,false, email,clefModule);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du message, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}
	
	@Override
	public void setTuteur(Integer clefModule, String email)
			throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_SET_TUTEUR,false, email,clefModule);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du message, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}

	@Override
	public void delete(Integer clefModule) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefModule);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression du module.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}

	@Override
	public ArrayList<Module> list() throws DAOException {
		ArrayList<Module> listeModules = new ArrayList<Module>();
		Connection cnx = null ;
		Statement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_VISIBLE_LIST);
			while(rs.next())
				listeModules.add(map(rs));
			return listeModules;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}		
	}
	
	private Module map(ResultSet rs) throws SQLException{
		Module module = new Module();
		module.setClefModule(rs.getInt("clefModule"));
		module.setNom(rs.getString("nom"));
		module.setDescription(rs.getString("description"));
		module.setNiveau(rs.getString("niveau"));
		module.setEstVisible(rs.getBoolean("estVisible"));
		module.setDateParution(rs.getDate("dateParution"));
		module.setResponsable(rs.getString("responsable"));
		module.setAuteur(rs.getString("auteur"));
		module.setTuteur(rs.getString("tuteur"));
		module.setTestFinal(rs.getInt("testFinal"));
		module.setImage(rs.getString("urlImage"));
		return module;
	}





}
