package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Enseignant;
import com.projectl.beans.Membre;

public interface MembreDAO {
	void create(Membre membre) throws DAOException, IllegalArgumentException;
	Membre read(String email) throws DAOException ;
	String readNom(String email) throws DAOException ;
	void update(String email,Membre membre) throws DAOException, IllegalArgumentException ;
	ArrayList<Membre> listEns() throws DAOException ;

}
