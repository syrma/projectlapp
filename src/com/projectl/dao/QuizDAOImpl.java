package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Quiz;

public class QuizDAOImpl implements QuizDAO {
	static final String SQL_CREATE = "INSERT INTO Quiz values(?);";
	static final String SQL_READ = "SELECT * FROM Quiz WHERE clefQuiz=?;";
	static final String SQL_LIST = "SELECT * FROM Quiz ;";

	static final String SQL_UPDATE = "UPDATE Quiz SET clefQuiz = ? ;";
	static final String SQL_DELETE = "DELETE FROM Quiz WHERE clefQuiz=?;";
	private DAOFactory factory ;
	
	public QuizDAOImpl(DAOFactory factory){
		this.factory = factory ;
	}
	@Override
	public int create(Quiz quiz) throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, true, quiz.getClefSeance());
			stmt.executeUpdate();
			ResultSet rs  = stmt.getGeneratedKeys();
			if(rs.next())
				return rs.getInt(1);
			else
				throw new DAOException("Échec de l'ajout du quiz.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Quiz read(Integer clefQuiz) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefQuiz);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void delete(Integer clefQuiz) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefQuiz);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de la séance.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public ArrayList<Quiz> list() throws DAOException {
		ArrayList<Quiz> liste = new ArrayList<Quiz>();
		Connection cnx = null ;
		Statement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_LIST);
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	private Quiz map(ResultSet rs) throws SQLException{
		Quiz quiz = new Quiz();
		quiz.setClefSeance(rs.getInt("clefQuiz"));		
		return quiz ;
	}
}
