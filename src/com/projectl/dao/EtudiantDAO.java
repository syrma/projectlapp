package com.projectl.dao;
import java.util.ArrayList;

import com.projectl.beans.Etudiant;
public interface EtudiantDAO {
boolean isEtudiant(String email,Integer clefModule) throws DAOException,IllegalArgumentException ;
boolean isEtudiant(String email) throws DAOException;
void linkToModule(String email, Integer clefModule) throws DAOException ;
void unlinkFromModule(String email,Integer clefModule) throws DAOException;
void unlinkAllFromModule(Integer clefModule) throws DAOException;

Integer readNote(String email,Integer clefModule) throws DAOException,IllegalArgumentException;
void setNote(String email, Integer clefModule, Integer note) throws DAOException;

Etudiant read(String email) throws DAOException,IllegalArgumentException;
void create(Etudiant etudiant) throws DAOException,IllegalArgumentException;
void delete(String email) throws DAOException ;
ArrayList<Etudiant> listEvaluerNiveau(Integer clefModule); 

}
