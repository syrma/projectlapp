package com.projectl.dao;
import java.util.ArrayList;

import com.projectl.beans.Test;

public interface TestDAO {
	void create(Test test,Integer clefModule) throws DAOException, IllegalArgumentException;
	Test read(Integer clefTest) throws DAOException ;
	void delete(Integer clefTest) throws DAOException, IllegalArgumentException ;
}
