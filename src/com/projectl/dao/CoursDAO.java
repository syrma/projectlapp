package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Cours;

public interface CoursDAO {
	int create(Cours cours) throws DAOException, IllegalArgumentException;
	Cours read(Integer clefCours) throws DAOException ;
	String readFaq(Integer clefCours) throws DAOException ;
 	void update(Integer clefCours,Cours cours) throws DAOException, IllegalArgumentException ;
	void updateFAQ(Integer clefCours, String faq) throws DAOException ;
	void delete(Integer clefCours) throws DAOException, IllegalArgumentException ;
	ArrayList<Cours> list(Integer clefModule) throws DAOException ;
}
