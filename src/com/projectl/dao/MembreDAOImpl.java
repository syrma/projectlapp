package com.projectl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Enseignant;
import com.projectl.beans.Membre;

import static com.projectl.dao.DAOUtilitaire.*;

class MembreDAOImpl implements MembreDAO {

	private static final String SQL_CREATE = "INSERT INTO Membre values(?,?,?,?,?,?,?,?,?);";
	private static final String SQL_READ = "SELECT * FROM Membre WHERE email=?;";
	private static final String SQL_READ_NOM = "SELECT nom,prenom FROM Membre WHERE email=?;";
	private static final String SQL_UPDATE = "UPDATE Membre SET nom = ?, prenom = ?, mdpasse = ?,"
			+ "estModerateur = ?, sexe = ? , dateNaissance = ?, dateInscription = ?, urlImage = ?"
			+ " WHERE email= ? ;";

	private static final String SQL_ENSEIGNANTS = "SELECT nom,prenom,Membre.email FROM Membre INNER JOIN Enseignant ON Membre.email=Enseignant.email;";
		
	DAOFactory factory ;

	public MembreDAOImpl(DAOFactory factory) {
		this.factory = factory;
	}

	@Override
	public void create(Membre membre) throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;

		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, false, membre.getNom(), membre.getPrenom(), membre.getMdpasse(), membre.getEmail(), membre.getEstModerateur(), membre.getSexe(), membre.getDateNaissance(), membre.getDateInscription(), membre.getAvatar());
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de l'ajout du nouveau membre.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public Membre read(String email) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, email);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public String readNom(String email) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, email);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return rs.getString("prenom") + " " + rs.getString("nom");
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void update(String email, Membre membre) throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			String sexe = null;
			System.out.println(membre);
			if(membre.getSexe()!=null)
				sexe = membre.getSexe().toString();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,membre.getNom(),membre.getPrenom(),membre.getMdpasse(),membre.getEstModerateur(),sexe,membre.getDateNaissance(),membre.getDateInscription(), membre.getAvatar(),email);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du message, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public ArrayList<Membre> listEns() throws DAOException {
		ArrayList<Membre> liste = new ArrayList<Membre>();
		Connection cnx = null ;
		Statement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_ENSEIGNANTS);
			while(rs.next())
				liste.add(mapIdentity(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}


	private static Membre mapIdentity(ResultSet rs) throws SQLException{
		Membre membre = new Membre();
		membre.setNom(rs.getString("nom"));
		membre.setPrenom(rs.getString("prenom"));
		membre.setEmail(rs.getString("email"));	
	
		return membre; 
	}
	
	private static Membre map(ResultSet rs) throws SQLException{
		Membre membre = new Membre();
		membre.setNom(rs.getString("nom"));
		membre.setPrenom(rs.getString("prenom"));
		membre.setMdpasse(rs.getString("mdpasse"));
		membre.setEmail(rs.getString("email"));	
		membre.setEstModerateur(rs.getBoolean("estModerateur"));
		membre.setDateNaissance(rs.getDate("dateNaissance"));
		membre.setDateInscription(rs.getDate("dateInscription"));

		return membre; 
	}


}
