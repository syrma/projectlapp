package com.projectl.dao;
import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Lecon;
import com.projectl.dao.LeconDAO;


public class LeconDAOImpl implements LeconDAO{
	static final String SQL_CREATE = "INSERT INTO Lecon values(?,?,?);";
	static final String SQL_READ = "SELECT * FROM Lecon WHERE clefLecon=?;";
	static final String SQL_LIST = "SELECT * FROM Lecon ;";

	static final String SQL_UPDATE = "UPDATE Lecon SET clefLecon = ?, urlFichier = ?, extension = ? ;";
	static final String SQL_DELETE = "DELETE FROM Lecon WHERE clefLecon=?;";
	private DAOFactory factory ;
	
	public LeconDAOImpl(DAOFactory factory){
		this.factory = factory ;
	}
	
	@Override
	public int create(Lecon lecon) throws DAOException,
			IllegalArgumentException {
		
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, true, lecon.getClefSeance(), lecon.getUrlFichier(), lecon.getExtension());
			stmt.executeUpdate();
			ResultSet rs  = stmt.getGeneratedKeys();
			if(rs.next())
				return rs.getInt(1);
			else
				throw new DAOException("Échec de l'ajout de la séance.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Lecon read(Integer clefLecon) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefLecon);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void update(Integer clefLecon, Lecon lecon) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false, lecon.getClefSeance(), lecon.getUrlFichier(), lecon.getExtension(), clefLecon);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification de la séance, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}

	@Override
	public void delete(Integer clefLecon) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefLecon);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de la séance.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}

	@Override
	public ArrayList<Lecon> list() throws DAOException {
		ArrayList<Lecon> liste = new ArrayList<Lecon>();
		Connection cnx = null ;
		Statement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_LIST);
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	private Lecon map(ResultSet rs) throws SQLException{
		Lecon lecon = new Lecon();
		lecon.setClefSeance(rs.getInt("clefLecon"));
		lecon.setUrlFichier(rs.getString("urlFichier"));
		lecon.setExtension(rs.getString("extension"));
		return lecon ;

	}
}
