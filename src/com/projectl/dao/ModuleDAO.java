package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Module;

public interface ModuleDAO {
	int create(Module module) throws DAOException, IllegalArgumentException;
	Module read(Integer clefModule) throws DAOException ;
	void update(Integer clefModule,Module module) throws DAOException, IllegalArgumentException ;
	void setAuteur(Integer clefModule,String email) throws DAOException, IllegalArgumentException ;
	void setTuteur(Integer clefModule,String email) throws DAOException, IllegalArgumentException ;
	void delete(Integer clefModule) throws DAOException, IllegalArgumentException ;
	ArrayList<String[]> index() throws DAOException;
	
	ArrayList<Module> list() throws DAOException ;
}
