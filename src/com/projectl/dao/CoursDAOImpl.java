package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Cours;

public class CoursDAOImpl implements CoursDAO {

	static final String SQL_CREATE = "INSERT INTO Cours(nom,indice,clefModule,faq) values(?,?,?,?);";
	static final String SQL_READ = "SELECT * FROM Cours WHERE clefCours=?;";
	static final String SQL_READ_FAQ = "SELECT faq FROM Cours WHERE clefCours=?;";

	static final String SQL_LIST = "SELECT * FROM Cours WHERE clefModule = ? ORDER BY indice ASC ;";
	
	static final String SQL_MAX_INDICE = "SELECT MAX(indice) FROM Cours WHERE clefModule = ? ;";
	static final String SQL_READ_INDICE = "SELECT * FROM Cours WHERE clefModule=? AND indice = ? ;";
	static final String SQL_REARRANGE_INDICE = "UPDATE Cours SET indice=indice+1 WHERE indice>= ? ;";

	static final String SQL_UPDATE = "UPDATE Cours SET nom = ?, clefModule = ?, faq = ?, WHERE clefCours = ?;";
	static final String SQL_UPDATE_FAQ = "UPDATE Cours SET faq = ? WHERE clefCours = ?;";

	static final String SQL_DELETE = "DELETE FROM Cours WHERE clefCours=?;";
	DAOFactory factory ;
	
	
	
	public CoursDAOImpl(DAOFactory factory) {
		this.factory = factory;
	}

	@Override
	public int create(Cours cours) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection();
			
			if(cours.getIndice()<0){
				stmt = initialisationRequetePreparee(cnx, SQL_MAX_INDICE, false, cours.getClefModule());
				rs = stmt.executeQuery();
				if(rs.next())
					cours.setIndice(rs.getInt(1)+1);
				rs.close();
				stmt.close();
			}else{
				stmt = initialisationRequetePreparee(cnx, SQL_READ_INDICE, false, cours.getClefModule(),cours.getIndice());
				rs = stmt.executeQuery();
				if(rs.next()){
					rs.close();
					stmt.close();
					stmt = initialisationRequetePreparee(cnx, SQL_REARRANGE_INDICE, false, cours.getIndice());
					stmt.executeUpdate();
				}
				if(rs!=null)
					rs.close();
				stmt.close();					
			}
				
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, true, cours.getNom(), cours.getIndice(), cours.getClefModule(), cours.getFaq());
			stmt.executeUpdate();
			rs  = stmt.getGeneratedKeys();
			if(rs.next())
				return rs.getInt(1);
			else
				throw new DAOException("Échec de l'ajout du nouveau cours.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Cours read(Integer clefCours) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefCours);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}
	
	@Override
	public String readFaq(Integer clefCours) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ_FAQ, false, clefCours);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return rs.getString("faq");
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}
	
	@Override
	public void update(Integer clefCours, Cours cours) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,cours.getNom(), cours.getIndice(), cours.getClefModule(), cours.getFaq(), clefCours);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du cours, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public void updateFAQ(Integer clefCours, String faq) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE_FAQ,false,faq, clefCours);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du cours, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}
	@Override
	public void delete(Integer clefCours) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefCours);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression du cours.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public ArrayList<Cours> list(Integer clefModule) throws DAOException {
		ArrayList<Cours> listeCours = new ArrayList<Cours>();
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_LIST, false, clefModule);
			rs = stmt.executeQuery();
			while(rs.next())
				listeCours.add(map(rs));
			return listeCours;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
	}
	}

	private Cours map(ResultSet rs) throws SQLException{
		Cours cours = new Cours();
		cours.setClefCours(rs.getInt("clefCours"));
		cours.setNom(rs.getString("nom"));
		cours.setClefModule(rs.getInt("clefModule"));
		cours.setFaq(rs.getString("faq"));
		return cours;
	}


}
