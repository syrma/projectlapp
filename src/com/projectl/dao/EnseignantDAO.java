package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Enseignant;

public interface EnseignantDAO {
		void create(Enseignant responsable) throws DAOException, IllegalArgumentException;
		Enseignant read(String email) throws DAOException ;
		Enseignant readResponsables(String email) throws DAOException ;
		void update(String email,Enseignant responsable) throws DAOException, IllegalArgumentException ;
		void delete(String email) throws DAOException ;
		ArrayList<Enseignant> list() throws DAOException ;

}
