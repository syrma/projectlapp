package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Seance;

public interface SeanceDAO {
	int create(Seance seance) throws DAOException, IllegalArgumentException;
	Seance read(Integer clefSeance) throws DAOException ;
	void update(Integer clefSeance,Seance seance) throws DAOException, IllegalArgumentException ;
	void delete(Integer clefSeance) throws DAOException, IllegalArgumentException ;
	ArrayList<Seance> list(Integer clefCours) throws DAOException ;
}
