package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Lecon;

public interface LeconDAO {
	int create(Lecon lecon) throws DAOException, IllegalArgumentException;
	Lecon read(Integer clefLecon) throws DAOException ;
	void update(Integer clefLecon,Lecon lecon) throws DAOException, IllegalArgumentException ;
	void delete(Integer clefLecon) throws DAOException, IllegalArgumentException ;
	ArrayList<Lecon> list() throws DAOException ;
}
