package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.projectl.beans.Lecon;
import com.projectl.beans.Quiz;
import com.projectl.beans.Seance;

public class SeanceDAOImpl implements SeanceDAO {
	static final String SQL_CREATE = "INSERT INTO Seance(clefCours,indice,nom,typeSeance) values(?,?,?,?);";
	static final String SQL_QUIZ = "INSERT INTO Quiz values(?);";
	static final String SQL_LECON = "INSERT INTO Lecon(clefLecon,urlFichier,extension) values(?,?,?);";
	
	static final String SQL_READ = "SELECT clefSeance,clefCours,indice,nom,typeSeance,urlFichier,extension FROM Seance LEFT JOIN Lecon ON clefSeance=clefLecon LEFT JOIN Quiz ON clefSeance = clefQuiz WHERE clefSeance=?;";
	static final String SQL_READ_LECON = "SELECT * FROM Lecon WHERE clefLecon=?;";
	
	static final String SQL_LIST = "SELECT clefSeance,clefCours,indice,nom,typeSeance,urlFichier,extension FROM Seance LEFT JOIN Lecon ON clefSeance=clefLecon LEFT JOIN Quiz ON clefSeance = clefQuiz WHERE clefCours= ? ORDER BY indice ASC ;";
	static final String SQL_UPDATE = "UPDATE Seance SET clefCours = ?, indice = ?, nom = ?, typeSeance = ? WHERE clefSeance = ? ;";
	
	static final String SQL_DELETE = "DELETE FROM Seance WHERE clefSeance=?;";
	static final String SQL_DELETE_QUIZ = "DELETE FROM Quiz WHERE clefQuiz=?;";
	static final String SQL_DELETE_LECON = "DELETE FROM Lecon WHERE clefLecon=?;";
	
	static final String SQL_MAX_INDICE = "SELECT MAX(indice) FROM Seance WHERE clefCours = ? ;";
	static final String SQL_READ_INDICE = "SELECT * FROM Seance WHERE clefCours=? AND indice = ? ;";
	static final String SQL_REARRANGE_INDICE = "UPDATE Seance SET indice=indice+1 WHERE indice>= ? ;";
	
	private DAOFactory factory ;
	
	public SeanceDAOImpl(DAOFactory factory) {
		this.factory = factory;
	}

	@Override
	public int create(Seance seance) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection();
			cnx.setAutoCommit(false);
			if(seance.getIndice()<0){
				stmt = initialisationRequetePreparee(cnx, SQL_MAX_INDICE, false, seance.getClefCours());
			rs = stmt.executeQuery();
			if(rs.next())
				seance.setIndice(rs.getInt(1)+1);
			rs.close();
			stmt.close();
		}else{
			stmt = initialisationRequetePreparee(cnx, SQL_READ_INDICE, false,seance.getClefCours(), seance.getIndice());
			rs = stmt.executeQuery();
			if(rs.next()){
				rs.close();
				stmt.close();
				stmt = initialisationRequetePreparee(cnx, SQL_REARRANGE_INDICE, false, seance.getIndice());
				stmt.executeUpdate();
			}
			if(rs!=null)
				rs.close();
			stmt.close();					
		}
			
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, true, seance.getClefCours(),seance.getIndice(),seance.getNom(),seance.getTypeSeance());
			stmt.executeUpdate();
			rs  = stmt.getGeneratedKeys();
			if(!rs.next())
				throw new DAOException("Échec de l'ajout de la séance.");			

			int id= rs.getInt(1);
			rs.close();
			stmt.close();
			if(seance instanceof Lecon){
				Lecon lecon = (Lecon) seance ;
				stmt= initialisationRequetePreparee(cnx, SQL_LECON, false, id,lecon.getUrlFichier(),lecon.getExtension());
			}else{
				stmt=initialisationRequetePreparee(cnx, SQL_QUIZ, false, id);
			}
			int i = stmt.executeUpdate();
			if(i==0){
				throw new DAOException("L'opération n'a pu être effectuée.");
			}else{
				cnx.commit();
			}
			return id;
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Seance read(Integer clefSeance) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefSeance);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void update(Integer clefSeance, Seance seance) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,seance.getClefCours(),seance.getIndice(),seance.getNom(),seance.getTypeSeance(), clefSeance);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du cours, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public void delete(Integer clefSeance) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE_LECON, false, clefSeance);
			stmt.close();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE_QUIZ, false, clefSeance);
			stmt.close();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefSeance);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de la séance.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public ArrayList<Seance> list(Integer clefCours) throws DAOException {
		ArrayList<Seance> liste = new ArrayList<Seance>();
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_LIST, false,clefCours);
			rs = stmt.executeQuery();
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			e.printStackTrace();
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	private Seance map(ResultSet rs) throws SQLException{
		String type = rs.getString("typeSeance");
		Seance seance ;
		if(type.equals("Lecon")){
			Lecon lecon =  new Lecon();
			lecon.setUrlFichier(rs.getString("urlFichier"));
			lecon.setExtension(rs.getString("extension"));
			seance=lecon;
		}else{
			seance = new Quiz();
		}
			seance.setClefSeance(rs.getInt("clefSeance"));
			seance.setClefCours(rs.getInt("clefCours"));
			seance.setIndice(rs.getInt("indice"));
			seance.setNom(rs.getString("nom"));
			seance.setTypeSeance(rs.getString("typeSeance"));
		return seance;
	}

}
