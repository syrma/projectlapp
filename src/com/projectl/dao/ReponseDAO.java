package com.projectl.dao;
import java.util.ArrayList;

import com.projectl.beans.Reponse;

public interface ReponseDAO {
	int create(Reponse response) throws DAOException, IllegalArgumentException;
	Reponse read(Integer clefReponse) throws DAOException ;
	void delete(Integer clefReponse) throws DAOException, IllegalArgumentException ;
	ArrayList<Reponse> list(Integer idQuestion) throws DAOException ;
}
