package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Reponse;

public class ReponseDAOImpl implements ReponseDAO {

	static final String SQL_CREATE = "INSERT INTO Reponse values(?,?,?);";
	static final String SQL_READ = "SELECT * FROM Reponse WHERE clefReponse=?;";
	static final String SQL_LIST = "SELECT * FROM Reponse  WHERE clefQuestion = ? ;";

	static final String SQL_UPDATE = "UPDATE Reponse SET clefReponse = ? ;";
	static final String SQL_DELETE = "DELETE FROM Reponse WHERE clefReponse=?;";
	private DAOFactory factory ;
	
	public ReponseDAOImpl(DAOFactory factory){
		this.factory = factory ;
	}
	@Override
	public int create(Reponse reponse) throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_CREATE, true, reponse.getClefReponse());
			stmt.executeUpdate();
			ResultSet rs  = stmt.getGeneratedKeys();
			if(rs.next())
				return rs.getInt(1);
			else
				throw new DAOException("Échec de l'ajout du test.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Reponse read(Integer clefTest) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefTest);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void delete(Integer clefReponse) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefReponse);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de la séance.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public ArrayList<Reponse> list(Integer clefQuestion) throws DAOException {
		ArrayList<Reponse> liste = new ArrayList<Reponse>();
		Connection cnx = null ;
		Statement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_LIST);
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	private Reponse map(ResultSet rs) throws SQLException{
		Reponse reponse = new Reponse();
		reponse.setClefReponse(rs.getInt("clefReponse"));
		reponse.setClefQuestion(rs.getInt("clefQuestion"));
		reponse.setContenu(rs.getString("contenu"));
		reponse.setEstCorrecte(rs.getBoolean("estCorrecte"));
		return reponse;
	}

}
