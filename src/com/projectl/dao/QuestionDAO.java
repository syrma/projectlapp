package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Question;

public interface QuestionDAO {
	int addToQuiz(Question question, Integer clefQuiz) throws DAOException, IllegalArgumentException;
	int addToTest(Question question, Integer clefTest) throws DAOException, IllegalArgumentException;
	Question read(Integer clefQuestion) throws DAOException ;
	void update(Integer clefQuestion,Question question) throws DAOException, IllegalArgumentException ;
	void delete(Integer clefQuestion) throws DAOException, IllegalArgumentException ;
	ArrayList<Question> list() throws DAOException ;
}
