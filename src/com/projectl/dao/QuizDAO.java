package com.projectl.dao;

import java.util.ArrayList;

import com.projectl.beans.Quiz;

public interface QuizDAO {
	int create(Quiz quiz) throws DAOException, IllegalArgumentException;
	Quiz read(Integer clefQuiz) throws DAOException ;
	void delete(Integer clefQuiz) throws DAOException, IllegalArgumentException ;
	ArrayList<Quiz> list() throws DAOException ;
}
