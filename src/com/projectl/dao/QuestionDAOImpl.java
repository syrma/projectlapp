package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Question;

public class QuestionDAOImpl implements QuestionDAO {
	static final String SQL_CREATE = "INSERT INTO Question(contenu) values(?);";
	
	static final String SQL_LINK_TO_QUIZ = "INSERT INTO QuestionQuiz values(?,?);";
	static final String SQL_READ = "SELECT * FROM Question WHERE clefQuestion=?;";
	static final String SQL_LIST = "SELECT * FROM Question ;";
	
	
	
	static final String SQL_UPDATE = "UPDATE Question SET contenu = ? WHERE clefQuestion = ? ;";
	static final String SQL_DELETE = "DELETE FROM Question WHERE clefQuestion=?;";
	private DAOFactory factory ;
	
	public QuestionDAOImpl(DAOFactory factory){
		this.factory = factory ; 
	}
	
	@Override
	public int addToQuiz(Question question, Integer clefQuiz)
			throws DAOException, IllegalArgumentException {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int addToTest(Question question, Integer clefTest)
			throws DAOException, IllegalArgumentException {
		// TODO Auto-generated method stub
		return 0;
	}

//	@Override
	public int create(Question question) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, true, question.getContenu());
			stmt.executeUpdate();
			ResultSet rs  = stmt.getGeneratedKeys();
			if(rs.next())
				return rs.getInt(1);
			else
				throw new DAOException("Échec de l'ajout de la question.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Question read(Integer clefQuestion) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, clefQuestion);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void update(Integer clefQuestion, Question question)
			throws DAOException, IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{ 
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,question.getContenu(), clefQuestion);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification de la question, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public void delete(Integer clefQuestion) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, clefQuestion);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de la séance.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}


	}

	@Override
	public ArrayList<Question> list() throws DAOException {
		ArrayList<Question> liste = new ArrayList<Question>();
		Connection cnx = null ;
		Statement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_LIST);
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}
	
	public Question map(ResultSet rs) throws SQLException{
		Question question = new Question();
		question.setClefQuestion(rs.getInt("clefQuestion"));
		question.setContenu(rs.getString("contenu"));
		return question;
	}

}
