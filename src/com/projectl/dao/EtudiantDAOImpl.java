package com.projectl.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.projectl.beans.Etudiant;
import com.projectl.beans.Membre;

public class EtudiantDAOImpl implements EtudiantDAO {

	private static final String SQL_CREATE = "INSERT INTO Etudiant VALUES (?,?) ;";
	private static final String SQL_IS_ETUDIANT_MODULE = "SELECT * FROM InscriptionModule WHERE email = ? AND clefModule = ? ;";
	private static final String SQL_IS_ETUDIANT = "SELECT * FROM InscriptionModule WHERE email = ? ;";
	private static final String SQL_LINK = "INSERT INTO InscriptionModule(email,clefModule) VALUES (?,?);";
	private static final String SQL_UNLINK = "DELETE FROM InscriptionModule WHERE email = ? AND clefModule = ? ;";
	private static final String SQL_UNLINK_ALL = "DELETE FROM InscriptionModule WHERE clefModule = ? ;";
	
	private static final String SQL_READ_NOTE = "SELECT note FROM InscriptionModule WHERE email=? AND clefModule = ? AND note IS NOT NULL;";
	private static final String SQL_SET_NOTE = "UPDATE InscriptionModule SET note = ? WHERE email = ? AND clefModule = ? ;";
		
	private static final String LISTE_ETUDIANTS_EVALUER_NIVEAU = "SELECT nom, prenom, Membre.email, note FROM Membre INNER JOIN InscriptionModule ON Membre.email=InscriptionModule.email WHERE clefModule = ? AND note IS NOT NULL ORDER BY note DESC ;";

	private static final String SQL_DELETE = "DELETE FROM Etudiant WHERE email=? ;";
	private static final String SQL_READ = "SELECT * FROM Etudiant WHERE email = ? ;";
	
	private DAOFactory factory ;
	
	public EtudiantDAOImpl(DAOFactory factory){
		this.factory = factory ;
	}
	
	@Override
	public boolean isEtudiant(String email, Integer clefModule) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_IS_ETUDIANT_MODULE, false, email,clefModule);
			rs = stmt.executeQuery();
			return rs.next();
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}


	@Override
	public boolean isEtudiant(String email) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_IS_ETUDIANT, false, email);
			rs = stmt.executeQuery();
			return rs.next();
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}
	@Override
	public void linkToModule(String email, Integer clefModule)
			throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_LINK, false, email,clefModule);
			int i = stmt.executeUpdate();
			if(i==0){
				throw new DAOException("Le lien n'a pas pu s'effectuer.");
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
		
	}
	
	@Override
	public void unlinkFromModule(String email, Integer clefModule)
			throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_UNLINK, false, email,clefModule);
			int i = stmt.executeUpdate();
			if(i==0){
				throw new DAOException("Le lien n'a pas pu être détruit, il n'existe peut-être pas.");
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
		
	}

	@Override
	public void unlinkAllFromModule(Integer clefModule)
			throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_UNLINK_ALL, false, clefModule);
			int i = stmt.executeUpdate();
			if(i==0){
				throw new DAOException("Les liens n'ont pas pu être détruits, ils n'existent peut-être pas.");
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
		
	}
	
	@Override
	public void create(Etudiant etudiant) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_CREATE, false, etudiant.getEmail(), etudiant.getNiveau());
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de l'ajout de l'étudiant.");			
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}
	
	@Override
	public void delete(String email) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_DELETE, false, email);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Échec de la suppression de l'enseignant.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}

	@Override
	public Etudiant read(String email) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, email);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}
	
	private Etudiant map(ResultSet rs) throws SQLException{
		Etudiant etudiant = new Etudiant();
		etudiant.setEmail(rs.getString("email"));
		etudiant.setNiveau(rs.getString("niveau"));
		return etudiant ;
	}
	
	private Etudiant mapIdentity(ResultSet rs) throws SQLException{
		Etudiant etudiant = new Etudiant();
		etudiant.setNom(rs.getString("nom"));
		etudiant.setPrenom(rs.getString("prenom"));
		etudiant.setEmail(rs.getString("email"));
		etudiant.setNote(rs.getInt("note"));
		return etudiant ;
	}

	@Override
	public ArrayList<Etudiant> listEvaluerNiveau(Integer clefModule) {
		ArrayList<Etudiant> liste = new ArrayList<Etudiant>();
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, LISTE_ETUDIANTS_EVALUER_NIVEAU, false, clefModule);
			rs = stmt.executeQuery();
			while(rs.next())
				liste.add(mapIdentity(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public Integer readNote(String email,Integer clefModule) throws DAOException,
			IllegalArgumentException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ_NOTE, false, email,clefModule);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			else{
				System.out.println(rs.getInt("note"));
				return rs.getInt("note");
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void setNote(String email, Integer clefModule, Integer note) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_SET_NOTE, false, note,email,clefModule);
			int i = stmt.executeUpdate();
			if(i==0){
				throw new DAOException("L'ajout de la note n'a pas pu s'effectuer.");
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
		
	}


}
