package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.beans.Etudiant;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.EtudiantDAO;


@WebServlet("/EvaluerNiveau")
public class EvaluerNiveauServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String ATTR_LISTE_ETUD = "listeEtudiants";

	private static final String VUE_EVALUER_NIVEAU = "WEB-INF/dialogues/pageEvaluerNiveau.jsp";

	private static final String ATTR_PLUS_DE_QUINZE = "plusDeQuinze";
	private static final String ATTR_PLUS_DE_DIX = "plusDeDix";
    
	private EtudiantDAO dao ;

	public void init() throws ServletException {
		DAOFactory factory = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		dao = factory.getEtudiantDAO() ;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			Integer id = new Integer(request.getParameter("id"));
			ArrayList<Etudiant> listeEtudiants = dao.listEvaluerNiveau(id);
				if(listeEtudiants.size()==0){
					response.sendError(404,"Aucun étudiant n'a encore passé le test.");
					return;
				}

			Float plusDeDix = (float) 0 ; 
			Float plusDeQuinze = (float) 0  ;
			for(Etudiant etudiant : listeEtudiants){
				if(etudiant.getNote()>=10){
					plusDeDix++;
					if(etudiant.getNote()>=15)
						plusDeQuinze++;
				}
			}
			plusDeDix = plusDeDix/listeEtudiants.size()*100;
			plusDeQuinze = plusDeQuinze/listeEtudiants.size()*100;

			request.setAttribute(ATTR_PLUS_DE_DIX, arrondi(plusDeDix,0));
			request.setAttribute(ATTR_PLUS_DE_QUINZE, arrondi(plusDeQuinze,0));
			request.setAttribute(ATTR_LISTE_ETUD, listeEtudiants);
			request.getRequestDispatcher(VUE_EVALUER_NIVEAU).forward(request,response);
		}catch(NumberFormatException e){
			response.sendError(400,"L'identifiant du module est invalide.");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}



	public static BigDecimal arrondi(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);       
        return bd;
    }


}