package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.beans.Module;
import com.projectl.beans.Question;
import com.projectl.beans.Reponse;
import com.projectl.beans.Test;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.ModuleDAO;
import com.projectl.dao.TestDAO;

/**
 * Servlet implementation class PosterInterrogationServlet
 */
@WebServlet("/PosterInterrogation")
public class PosterInterrogationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_POSTER_TEST = "WEB-INF/dialogues/pagePosterTest.jsp";

	private static final String PARAM_ID = "id";

	private static final String VUE_AUTH = "Authentification?auth=yes";

	private static final String PAGE_MODULE = "InscriptionModule?id=";

	private ModuleDAO dao ; 
	private TestDAO daoTest ; 

	public void init(){
		DAOFactory factory = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		dao = factory.getModuleDAO() ;
		daoTest = factory.getTestDAO() ;
	}



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = (String) request.getSession().getAttribute("email");
		if(user == null){
			response.sendRedirect(VUE_AUTH);
			return;
		}
		try{
			Integer idModule = new Integer(request.getParameter(PARAM_ID));
			Module mod = dao.read(idModule);
			if(mod == null){
				response.sendError(404,"Vous essayez d'accéder à un module qui n'existe pas.");
				return;
			}
			if(!mod.getResponsable().equals(user)){
				response.sendError(403,"Vous n'avez pas l'accès de responsable pour accéder à cette fonctionnalité");
				return ;
			}
			request.getRequestDispatcher(VUE_POSTER_TEST).forward(request,response);
		}catch(NumberFormatException e){
			response.sendError(400,"L'identifiant du module est invalide.");
		}

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer clefModule ;
		try{
			clefModule = new Integer(request.getParameter(PARAM_ID));
		}catch(NumberFormatException e){
			response.sendError(400,"L'identifiant du module est invalide.");
			return;
		}

		Enumeration<String> inputs = request.getParameterNames();
		String elem ;

		ArrayList<Question> questions = new ArrayList<Question>();
		Question question ;
		ArrayList<Reponse> reponses = null ;

		Reponse reponse = null ;
		while(inputs.hasMoreElements()){
			elem = inputs.nextElement();
			System.out.println(elem);
			if(request.getParameter(elem).equals("")){
				request.setAttribute("erreur","Veuillez remplir tous les champs");
				request.getRequestDispatcher(VUE_POSTER_TEST).forward(request, response);
				return;
			}

			if(elem.startsWith("question") && request.getParameter(elem) != null){
				question = new Question();
				question.setContenu(request.getParameter(elem)); 
				reponses = new ArrayList<Reponse>();
				questions.add(question);
				question.setReponses(reponses);
			}else if(elem.startsWith("reponse")){
				reponse = new Reponse();
				reponse.setContenu(request.getParameter(elem));
				reponse.setEstCorrecte(false);
				reponses.add(reponse);
			}else if(elem.startsWith("valeur-reponse")){
				reponse.setEstCorrecte(true);
			}
		}
		
		for(Question questionn : questions){
			System.out.println(questionn.getContenu());
		}
		
		Test test = new Test();
		test.setQuestions(questions);
		
		daoTest.create(test, clefModule);
		response.sendRedirect(PAGE_MODULE+clefModule);
	}

}
