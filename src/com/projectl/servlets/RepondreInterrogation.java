package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.beans.Question;
import com.projectl.beans.Reponse;
import com.projectl.beans.Test;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.EtudiantDAO;
import com.projectl.dao.ModuleDAO;
import com.projectl.dao.TestDAO;

/**
 * Servlet implementation class RepondreInterrogation
 */
@WebServlet("/RepondreInterrogation")
public class RepondreInterrogation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String PARAM_ID = "id";

	private static final String VUE_AUTH = "Authentification?auth=yes";

	private static final String ATTR_TEST = "test";

	private static final String VUE_REPONDRE_TEST = "pageRepondreTest.jsp";

	private TestDAO dao ; 
	private ModuleDAO daoMod ;
	private EtudiantDAO daoEtud;

	public void init(){
		DAOFactory factory = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		dao = factory.getTestDAO() ;
		daoMod = factory.getModuleDAO();
		daoEtud = factory.getEtudiantDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email") == null){
			response.sendRedirect(VUE_AUTH);
			return;
		}
		try{
			Integer idModule = new Integer(request.getParameter(PARAM_ID));
			Test test = dao.read(daoMod.read(idModule).getTestFinal());
			if(test == null){
				response.sendError(404,"Désolé, ce module n'a pas encore de test final.");
				return;
			}

			request.setAttribute(ATTR_TEST, test);
			request.getRequestDispatcher(VUE_REPONDRE_TEST).forward(request,response);


		}catch(NumberFormatException e){
			response.sendError(400,"Identifiant invalide du module");
			return ;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userMail = (String) request.getSession().getAttribute("email");
		if(userMail == null){
			response.sendRedirect(VUE_AUTH);
			return ;
		}
		try{
			Integer clefModule = new Integer(request.getParameter("id"));
			if(daoEtud.readNote(userMail, clefModule)!=null){
			//	System.out.println(daoEtud.readNote(userMail, clefModule));
				response.sendError(403,"Vous avez déjà passé ce test.");
				return;
			}

			Integer clefTest = new Integer(request.getParameter("idTest"));
			Test test = dao.read(clefTest);
			Map<Integer,Boolean> resultat = new HashMap<Integer,Boolean>();
			int note = 0;
			for(Question question : test.getQuestions()){
				for(Reponse reponse : question.getReponses()){
					Integer clefReponse = reponse.getClefReponse();
					Boolean reponseEtudiant = new Boolean(request.getParameter(clefReponse.toString()));
					Boolean reponseCorrecte = (reponseEtudiant.equals(reponse.getEstCorrecte()));
					if(reponseCorrecte)
						note++;
					resultat.put(clefReponse, reponseCorrecte);
				}
			}
			int noteMax = resultat.size();
			note = (note * 20)/noteMax;
			
			daoEtud.setNote(userMail, clefModule, note);
			request.setAttribute("test", test);
			request.setAttribute("resultat", resultat);
			request.setAttribute("note", note);
			request.getRequestDispatcher(VUE_REPONDRE_TEST).forward(request, response);

		}catch(NumberFormatException e){
			response.sendError(400,"Ce test est invalide.");
			return;
		}



	}

}
