package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.controls.MembreCtr;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.MembreDAO;

/**
 * Servlet implementation class ModifProfilServlet
 */
@WebServlet("/Modification")
public class ModifProfilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String VUE = "/WEB-INF/dialogues/modifierProfil.jsp";
	private static final String AUTH = "/ProjetLicence/Authentification";
	
	private MembreDAO dao ;
	
	public void init(){
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getMembreDAO() ;
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MembreCtr control = new MembreCtr(dao, request);
		control.modifier();
		request.getRequestDispatcher(VUE).forward(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")!=null)
			request.getRequestDispatcher(VUE).forward(request, response);
		else
			response.sendRedirect(AUTH);
	}

}