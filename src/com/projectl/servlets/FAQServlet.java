package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.dao.CoursDAO;
import com.projectl.dao.DAOException;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.ModuleDAO;

/**
 * Servlet implementation class FAQServlet
 */
@WebServlet("/FAQ")
public class FAQServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static final String PARAM_FAQ="faq";
	private static final String PARAM_MODULE="id";
	private static final String PARAM_COURS="idCours";

	
	private static final String ATTR_FAQ="faq";
	
	private static final String VUE_GESTION_FAQ="WEB-INF/dialogues/gestionFAQ.jsp";
	private static final String VUE_PAGE_MODULE="InscriptionModule?id=";
	private static final String VUE_AUTH="Authentification?auth=yes";

	private static final String ATTR_ERREUR = "erreur";
		
	private CoursDAO dao ;
	private ModuleDAO daoMod ; 
	
	@Override
	public void init() throws ServletException {
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getCoursDAO();
		daoMod = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getModuleDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mail = (String) request.getSession().getAttribute("email");
		Integer id,idCours ; 
		try{
			id = new Integer(request.getParameter(PARAM_MODULE));
			idCours = new Integer(request.getParameter(PARAM_COURS));
		}catch(NumberFormatException e)
		{
			e.printStackTrace();
			request.setAttribute(ATTR_ERREUR,"L'identifiant du module ou du cours est invalide.");
			request.getRequestDispatcher(VUE_GESTION_FAQ).forward(request, response);
			return;
		}
		if(mail==null){
			request.getRequestDispatcher(VUE_AUTH).forward(request, response);
			return;
		}
		if(mail.equals(daoMod.read(id).getTuteur())){ 
			request.setAttribute(ATTR_FAQ, dao.readFaq(idCours));
			request.getRequestDispatcher(VUE_GESTION_FAQ).forward(request, response);			
		}else{
			response.sendError(403, "Vous n'avez pas l'accès de tuteur pour gérer la FAQ.");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			String faq = request.getParameter(PARAM_FAQ);
			Integer idCours = new Integer(request.getParameter(PARAM_COURS));
			Integer idModule = new Integer(request.getParameter(PARAM_MODULE));
			dao.updateFAQ(idCours, faq);
			response.sendRedirect(VUE_PAGE_MODULE+idModule);
		}catch(NumberFormatException e){
			e.printStackTrace();
			request.setAttribute(ATTR_ERREUR,"L'identifiant du module ou du cours est invalide.");
			request.getRequestDispatcher(VUE_GESTION_FAQ).forward(request, response);
			return;
		}catch(DAOException e){
			e.printStackTrace();
			request.setAttribute(ATTR_ERREUR,"Une erreur a eu lieu, merci de réessayer.");
			request.getRequestDispatcher(VUE_GESTION_FAQ).forward(request, response);
		}
		
	}

}
