package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.forum.beans.Topic;
import com.forum.dao.TopicDAO;
import com.projectl.beans.ContenuAdditionnel;
import com.projectl.beans.Cours;
import com.projectl.beans.Lecon;
import com.projectl.beans.Module;
import com.projectl.beans.Seance;
import com.projectl.controls.ModuleCtr;
import com.projectl.dao.ContenuAdditionnelDAO;
import com.projectl.dao.CoursDAO;
import com.projectl.dao.DAOException;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.EtudiantDAO;
import com.projectl.dao.ModuleDAO;
import com.projectl.dao.SeanceDAO;

/**
 * Servlet implementation class InscriptionModule
 */
@WebServlet("/InscriptionModule")
public class InscriptionModule extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private EtudiantDAO etudiantDAO ;
	private ModuleDAO moduleDAO ;
	private CoursDAO coursDAO ;
	private SeanceDAO seanceDAO ;
	private ContenuAdditionnelDAO caDAO ;
	private TopicDAO topicDAO ; 

	private static final String ATTRIBUT_SESSION = "email";
	private static final String ATTRIBUT_ERREUR = "error";
	private static final String ATTRIBUT_ID = "id";
	private static final String ATTRIBUT_INSCRIPTION = "inscription";

	private static final String ATTRIBUT_LISTE_COURS = "listeCours";
	private static final String ATTRIBUT_LISTE_SEANCE = "listeSeances";
	private static final String ATTRIBUT_LISTE_CA = "listeCA";
	private static final String ATTRIBUT_LISTE_TOPIC = "listeTopics";
	private static final String ATTRIBUT_COURS_COURANT = "coursCourant";
	private static final String ATTRIBUT_SEANCE_COURANTE = "seanceCourante";
	private static final String ATTRIBUT_NOM_MODULE = "nomModule";


	private static final String VUE_AUTH = "/ProjetLicence/Authentification?auth=yes";
	private static final String VUE_PAGE_MODULE_ETUD = "WEB-INF/dialogues/pageModuleEtudiant.jsp";
	private static final String VUE_PAGE_MODULE_AUTEUR= "WEB-INF/dialogues/pageModuleAuteur.jsp";
	private static final String VUE_PAGE_MODULE_TUTEUR = "WEB-INF/dialogues/pageModuleTuteur.jsp";
	private static final String VUE_PAGE_MODULE_RESP = "WEB-INF/dialogues/pageModuleResponsable.jsp";

	private static final String VUES_PAGE_MODULE = "InscriptionModule?id=";
	private static final String VUE_PAGE_DESCRIPTION = "Module?id=";
	private static final String ATTRIBUT_MIME_TYPE = "mimeType";


	@Override
	public void init() throws ServletException {
		DAOFactory factory = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		etudiantDAO	= factory.getEtudiantDAO();
		moduleDAO = factory.getModuleDAO();
		coursDAO = factory.getCoursDAO();
		seanceDAO = factory.getSeanceDAO();
		topicDAO = factory.getTopicDAO();
		caDAO = factory.getContenuAdditionnelDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = (String) request.getSession().getAttribute(ATTRIBUT_SESSION);
		if(email==null){
			response.sendRedirect(VUE_AUTH);
		}else{
			Integer id ;
			Module mod ;
			try{
				id = new Integer(request.getParameter(ATTRIBUT_ID));
				mod = moduleDAO.read(id);
				if(mod==null){
					response.sendError(404,"Le module demandé n'existe pas.");
					return;
				}
				request.setAttribute(ATTRIBUT_NOM_MODULE,mod.getNom());
			}catch(NumberFormatException e){
				response.sendError(400,"Erreur durant l'identification du module demandé (identifiant invalide).");
				return;
			}
			
				if(!etudiantDAO.isEtudiant(email,id) && !email.equals(mod.getAuteur()) && !email.equals(mod.getTuteur()) && !email.equals(mod.getResponsable())){
				request.setAttribute(ATTRIBUT_ERREUR, "Vous n'êtes pas inscrit à ce module, veuillez vous inscrire.");
				request.getRequestDispatcher(VUE_PAGE_DESCRIPTION+id).forward(request, response);
				return;
			}

			Cours coursCourant = null ;
			Seance seanceCourante = null ;
			
			Integer idCours = null ;
			Integer idSeance = null ;
			try{
				idCours = new Integer(request.getParameter("idCours"));
				idSeance = new Integer(request.getParameter("idSeance"));
			}catch(NumberFormatException e){}
			
			ArrayList<Cours> listeCours = coursDAO.list(id);
			ArrayList<Seance> listeSeances = null;
			ArrayList<Topic> listeTopics = null;
			ArrayList<ContenuAdditionnel> listeCA = null;
			
			if(idCours == null && listeCours.size() != 0)
				idCours = listeCours.get(0).getClefCours();
			
			if(idCours != null){
				coursCourant = coursDAO.read(idCours);
				listeSeances = seanceDAO.list(idCours);
				if(idSeance == null && listeSeances.size() != 0)
					idSeance = listeSeances.get(0).getClefSeance();
				
				if(idSeance != null){
					seanceCourante = seanceDAO.read(idSeance);
					listeCA = caDAO.list(idSeance);
					listeTopics = topicDAO.list(idSeance);
				}
			}
			request.setAttribute(ATTRIBUT_COURS_COURANT, coursCourant);
			request.setAttribute(ATTRIBUT_SEANCE_COURANTE, seanceCourante);
			request.setAttribute(ATTRIBUT_LISTE_COURS, listeCours);
			request.setAttribute(ATTRIBUT_LISTE_SEANCE, listeSeances);
			request.setAttribute(ATTRIBUT_LISTE_CA, listeCA);
			request.setAttribute(ATTRIBUT_LISTE_TOPIC, listeTopics);
			
			if(seanceCourante instanceof Lecon){
				Lecon lecon = (Lecon) seanceCourante;
				request.setAttribute(ATTRIBUT_MIME_TYPE,getServletContext().getMimeType((lecon.getUrlFichier()+"."+lecon.getExtension())));
			}
			
			if(etudiantDAO.isEtudiant(email,id)){
				request.getRequestDispatcher(VUE_PAGE_MODULE_ETUD).forward(request, response);
				return;
			}

			if(email.equals(mod.getAuteur())){
				request.getRequestDispatcher(VUE_PAGE_MODULE_AUTEUR).forward(request, response);
				return;
			}

			if(email.equals(mod.getTuteur())){
				request.getRequestDispatcher(VUE_PAGE_MODULE_TUTEUR).forward(request, response);
				return;
			}

			if(email.equals(mod.getResponsable())){
				request.getRequestDispatcher(VUE_PAGE_MODULE_RESP).forward(request, response);
				return;
			}




		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = (String) request.getSession().getAttribute(ATTRIBUT_SESSION);
		Integer id = null ;
		try{
			id = new Integer(request.getParameter(ATTRIBUT_ID));
		}catch(NumberFormatException e){
			response.sendError(406,"Une erreur est survenue lors de la recherche du module. (identifiant du module invalide)");
			return;
		}

		if(email==null){
			response.sendRedirect(VUE_AUTH);
			return;
		}

		if(new Boolean(request.getParameter(ATTRIBUT_INSCRIPTION))){

			Module module = moduleDAO.read(id);
			if((email.equals(module.getResponsable()))||(email.equals(module.getAuteur()))||(email.equals(module.getTuteur()))){
				response.sendRedirect(VUES_PAGE_MODULE+id);
				return;
			}

			try{
				new ModuleCtr(email).inscrireEtudiant(etudiantDAO,id);
			}catch(DAOException e){
				e.printStackTrace();
				response.sendError(505,"Le module auquel vous avez essayé de vous inscrire n'existe pas ou plus.");
			}
		//	request.getRequestDispatcher(VUE_PAGE_MODULE_ETUD).forward(request, response);
			doGet(request,response);
			return;
		}
		
		new ModuleCtr(email).desinscrireEtudiant(etudiantDAO, id);
		response.sendRedirect(VUE_PAGE_DESCRIPTION+id);
	}

}
