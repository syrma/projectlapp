package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections; 
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.beans.Module;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.ModuleDAO;

/**
 * Servlet implementation class Accueil
 */
@WebServlet("/Accueil")
public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ModuleDAO dao ;
	private static String VUE = "/WEB-INF/dialogues/index.jsp";
	private static String ATTR_LISTE = "listeModules";
	
	public void init(){
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getModuleDAO() ;
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Module> listeModules = dao.list();
		Collections.sort(listeModules,new Comparator<Module>(){
			@Override
	        public int compare(Module mod1, Module mod2){
				return mod2.getDateParution().compareTo(mod1.getDateParution());
			}
		});
		request.setAttribute(ATTR_LISTE,listeModules.subList(0, Math.min(listeModules.size(),4)));
		request.getRequestDispatcher(VUE).forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
