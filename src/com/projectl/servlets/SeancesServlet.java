package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;

import com.projectl.beans.Lecon;
import com.projectl.beans.Seance;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.SeanceDAO;

@MultipartConfig
@WebServlet("/Seance")
public class SeancesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Path CHEMIN_CONTENU = Paths.get(System.getProperty("user.home"),"uploads") ;


	private static final String PARAM_NOM = "nom" ;
	private static final String PARAM_ID_COURS = "idCours";
	private static final String PARAM_ID_SEANCE = "idSeance";
	private static final String PARAM_INDICE = "indice";
	private static final String PARAM_TYPE = "seanceType" ;
	private static final String PARAM_FICHIER = "fichier" ;

	private static final String VUE_CREA = "/WEB-INF/dialogues/pageModifierCours.jsp";
	private static final String VUE_MODULE = "InscriptionModule?id=";
	private static final String VUE_GESTION_COURS="GererCours?idCours=";
	private static final String VUE_AUTH="Authentification?auth=yes";

	private SeanceDAO dao ;

	public void init() throws ServletException {
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getSeanceDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")==null){
			response.sendRedirect(VUE_AUTH);
			return;
		}
		
		if(request.getParameter(PARAM_ID_SEANCE)==null){
			request.getRequestDispatcher(VUE_GESTION_COURS).forward(request,response);
			return;
		}

		try{
			Integer id = new Integer(request.getParameter(PARAM_ID_SEANCE));
			Lecon lecon = (Lecon) dao.read(id);
			String filename = lecon.getNom()+"."+lecon.getExtension();
			File file = new File(CHEMIN_CONTENU.toFile(), lecon.getUrlFichier()+"."+lecon.getExtension());
			response.setHeader("Content-Type", getServletContext().getMimeType(filename));
			response.setHeader("Content-Length", String.valueOf(file.length()));
			response.setHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
			Files.copy(file.toPath(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")==null){
			response.sendRedirect(VUE_AUTH);
			return;
		}
		ArrayList<String> erreurs = new ArrayList<String>();
		Integer idCours;
		try{
			idCours = new Integer(request.getParameter(PARAM_ID_COURS));
		}catch(NumberFormatException e){
			e.printStackTrace();
			try{
				Integer id = (Integer) request.getSession().getAttribute("id"); 
				request.getRequestDispatcher(VUE_MODULE+id);
				return;
			}catch(NumberFormatException ee){
				e.printStackTrace();
				response.sendRedirect(VUE_AUTH);
				return;
			}
		}
		try{
			String nom = request.getParameter(PARAM_NOM);
			Integer indice ;
			try{
				indice = new Integer(request.getParameter(PARAM_INDICE));
			}catch(NumberFormatException e){
				indice = -1 ;
			}
			Part part = request.getPart(PARAM_FICHIER);
			String expectedMimeType = request.getParameter(PARAM_TYPE) ; 

			if(nom == null){
				erreurs.add("Veuillez entrer un nom.");
			}

			String fileName = extractFileName(part);

			if(fileName.equals("")){
				erreurs.add("Veuillez entrer un fichier.");
			}else{			
				String extension = FilenameUtils.getExtension(fileName);
				String mimeType = getServletContext().getMimeType(fileName);
				if(expectedMimeType.equals("presentation"))
					expectedMimeType = "application/pdf";
				else if(expectedMimeType.equals("video"))
					expectedMimeType = "video/";

				if(!mimeType.startsWith(expectedMimeType)){
					erreurs.add("L'extension de votre fichier n'est pas supportée.");
				}

				if(erreurs.size()==0){
					Path fichier = Files.createTempFile(CHEMIN_CONTENU, "seance-", "."+extension);
					part.write(fichier.toString());

					Lecon lecon = new Lecon(); 
					lecon.setClefCours(idCours);
					lecon.setNom(nom);
					lecon.setExtension(extension);
					lecon.setIndice(indice);
					lecon.setUrlFichier(FilenameUtils.removeExtension(fichier.getFileName().toString()));
					lecon.setTypeSeance("Lecon");
					dao.create(lecon);
				}
			}
		}catch(IOException e){
			e.printStackTrace();
			erreurs.add("Une erreur a eu lieu pendant la lecture du fichier.");
		}catch(NumberFormatException e){
			e.printStackTrace();
			erreurs.add("L'identifiant du cours ou l'indice de la séance est invalide.");
		}
		
		request.setAttribute("erreurs",erreurs);
		request.getRequestDispatcher(VUE_GESTION_COURS+idCours).forward(request, response);
		//	response.sendRedirect(VUE_GESTION_COURS);
	}

	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}

}
