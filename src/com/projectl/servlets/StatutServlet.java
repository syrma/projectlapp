package com.projectl.servlets;
import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.dao.DAOFactory;
import com.projectl.dao.ModuleDAO;


@WebServlet("/Statut")
public class StatutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String VUE_DESC_MODULE = "Module?id=";
	private static final String VUE_ACCUEIL = "Accueil";

	private static final String PARAM_ID = "id";
	private static final String PARAM_MAIL = "enseignant";
	private static final String PARAM_SOURCE = "source";

	private ModuleDAO dao ;

	public void init() throws ServletException {
		DAOFactory daofactory = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		dao = daofactory.getModuleDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(VUE_ACCUEIL);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter(PARAM_MAIL);
		Integer id ;
		try{
			id = new Integer(request.getParameter(PARAM_ID));
		}catch(NumberFormatException e){
			response.sendError(400,"L'identifiant du module est invalide.");
			return;
		}
		if(!dao.read(id).getResponsable().equals((String) request.getSession().getAttribute("email"))){
			response.sendError(403,"Vous n'avez pas l'accès de responsable de module pour effectuer cette opération.");
			return;
		}

		if(email != null && request.getParameter(PARAM_SOURCE).equals("tuteur"))
			dao.setTuteur(id, email);
		else if(email != null && request.getParameter(PARAM_SOURCE).equals("auteur"))
			dao.setAuteur(id, email);

		response.sendRedirect(VUE_DESC_MODULE+id);
		return;
	}

}
