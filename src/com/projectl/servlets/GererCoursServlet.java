package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.beans.Seance;
import com.projectl.dao.ContenuAdditionnelDAO;
import com.projectl.dao.CoursDAO;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.SeanceDAO;

/**
 * Servlet implementation class GererCoursServlet
 */
@WebServlet("/GererCours")
public class GererCoursServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Path CHEMIN_CONTENU = Paths.get(System.getProperty("user.home"),"uploads") ;
	private static final String PARAM_NOM = "nom" ;
	private static final String PARAM_SEANCE = "idSeance" ;
	private static final String PARAM_ID = "idCours";
	private static final String PARAM_CREA = "create";
	
	private static final String VUE_CREA = "/WEB-INF/dialogues/pageModifierCours.jsp";
	private static final String VUE_MODULE = "InscriptionModule?idCours=";

	private static final String ATTR_LISTE_SEANCE = "listeSeances";


	private ContenuAdditionnelDAO daoCA ;
	private CoursDAO daoCours ;
	private SeanceDAO daoSeance ;


	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		DAOFactory dao = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		daoCA = dao.getContenuAdditionnelDAO();
		daoCours = dao.getCoursDAO();
		daoSeance = dao.getSeanceDAO();
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			Integer clefCours = new Integer(request.getParameter(PARAM_ID));	
			ArrayList<Seance> listeSeances = daoSeance.list(clefCours);
			request.setAttribute(ATTR_LISTE_SEANCE, listeSeances);
			System.out.println("gerercours");
			request.getRequestDispatcher(VUE_CREA).forward(request, response);
		}catch(NumberFormatException e){
			response.sendError(400,"L'identifiant du cours est invalide.");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getAttribute("erreurs") != null)
			doGet(request,response);
	}

}
