package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;

import com.projectl.beans.ContenuAdditionnel;
import com.projectl.dao.ContenuAdditionnelDAO;
import com.projectl.dao.DAOFactory;

/**
 * Servlet implementation class CAServlet
 */

@MultipartConfig
@WebServlet("/Contenu")
public class CAServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Path CHEMIN_CONTENU = Paths.get(System.getProperty("user.home"),"uploads") ;
	private static final String PARAM_NOM = "nom" ;
	private static final String PARAM_SEANCE = "idSeance" ;
	private static final String PARAM_ID = "idca";
	private static final String PARAM_CREA = "create";

	private static final String VUE_CREA = "/WEB-INF/dialogues/pageModifierCours.jsp";
	private static final String VUE_MODULE = "InscriptionModule";
	private static final String VUE_GESTION_COURS="GererCours?idCours=1";
	private static final String VUE_AUTH="Authentification?auth=yes";

	private ContenuAdditionnelDAO dao ;


	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getContenuAdditionnelDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")==null){
			response.sendRedirect(VUE_AUTH);
			return;
		}
		
		if(request.getParameter(PARAM_ID)==null){
			request.getRequestDispatcher(VUE_GESTION_COURS).forward(request,response);
			return;
		}

		try{
			Integer id = new Integer(request.getParameter(PARAM_ID));
			ContenuAdditionnel ca = dao.read(id);
//			String filename = request.getPathInfo().substring(1);
			System.out.println(ca.getExtension());
			String filename = ca.getNom()+"."+ca.getExtension();
			File file = new File(CHEMIN_CONTENU.toFile(), ca.getUrlFichier()+"."+ca.getExtension());
			response.setHeader("Content-Type", getServletContext().getMimeType(filename));
			response.setHeader("Content-Length", String.valueOf(file.length()));
			response.setHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
			Files.copy(file.toPath(), response.getOutputStream());
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")==null){
			response.sendRedirect(VUE_AUTH);
			return;
		}
		
		ArrayList<String> erreurs = new ArrayList<String>();
		try{
			Part part = request.getPart("fichier");
			String fileName = extractFileName(part);
			Integer clefSeance = new Integer(request.getParameter(PARAM_SEANCE));
			if(fileName.equals("")){
				erreurs.add("Veuillez entrer un fichier.");
			}else{
				String extension = FilenameUtils.getExtension(fileName);

				Path fichier = Files.createTempFile(CHEMIN_CONTENU, "ca-", "."+extension);
				part.write(fichier.toString());
				ContenuAdditionnel ca = new ContenuAdditionnel();
				ca.setUrlFichier(FilenameUtils.removeExtension(fichier.getFileName().toString()));
				ca.setExtension(extension);
				ca.setSeance(clefSeance);
				ca.setNom(FilenameUtils.getBaseName(fileName));
				dao.create(ca);
			}


		}catch(IOException e){
			e.printStackTrace();
			erreurs.add("Une erreur a eu lieu pendant la lecture du fichier.");
		}catch(NumberFormatException e){
			e.printStackTrace();
			erreurs.add("L'identifiant de la séance est invalide.");
		}
		request.setAttribute("erreurs",erreurs);
		request.getRequestDispatcher(VUE_GESTION_COURS).forward(request, response);
		//	response.sendRedirect(VUE_GESTION_COURS);
	}

	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}
}
