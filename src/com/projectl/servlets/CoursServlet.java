package com.projectl.servlets;
import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectl.controls.CoursCtr;
import com.projectl.dao.CoursDAO;
import com.projectl.dao.DAOException;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.ModuleDAO;

/**
 * Servlet implementation class CoursServlet
 */
@WebServlet("/Cours")
public class CoursServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static final String PARAM_ID_MODULE = "id";
	private static final String PARAM_INDICE = "indice";

	private static final String VUE_PAGE_MODULE = "/ProjetLicence/InscriptionModule?id=";
	private static final String VUE_PAGE_CREATION_COURS = "WEB-INF/dialogues/creationCours.jsp";


	
    private CoursDAO dao ;
    private ModuleDAO daoM; 
    

	@Override
	public void init() throws ServletException {
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getCoursDAO();
		daoM = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getModuleDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(VUE_PAGE_CREATION_COURS).forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mail = (String) request.getSession().getAttribute("email");
		System.out.println(request.getParameter("id"));
		try{
			Integer idModule = new Integer(request.getParameter(PARAM_ID_MODULE));
			Integer indice ;
			try{
				indice = new Integer(request.getParameter(PARAM_INDICE));
			}catch(NumberFormatException e){
				indice=-1;
			}

			if(mail != null && mail.equals(daoM.read(idModule).getAuteur())){
				int idCours = new CoursCtr(dao).creerCours(idModule,indice,request);
				
				if(idCours > 0)
					response.sendRedirect((VUE_PAGE_MODULE+idModule+"&idCours="+idCours));
				else
					request.getRequestDispatcher(VUE_PAGE_CREATION_COURS).forward(request, response);			
			}else{
				response.sendError(403, "Vous n'avez pas l'accès d'auteur pour la création d'un cours.");
			}
		}catch(NumberFormatException e){
			response.sendError(400,"Le module demandé est invalide.");
		}catch(DAOException e){
			response.sendError(500,"une erreur est survenue lors de la création.");
		}
	}

}
