package com.projectl.servlets;

import static com.projectl.config.Initialisateur.*;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;

import com.projectl.beans.Module;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.ModuleDAO;
/**
 * Servlet implementation class RechercheServlet
 */
@WebServlet("/Recherche")
public class RechercheServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static String VUE_LISTE = "/WEB-INF/dialogues/listeModules.jsp";

	private static String ATTR_LISTE = "listeModules";
	private static String ATTR_RECHERCHE = "resultatRecherche";


	private ModuleDAO dao;
	private StandardAnalyzer analyzer ; 
	private Directory index ;

	@Override
	public void init() throws ServletException {
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getModuleDAO();
		index = (Directory) getServletContext().getAttribute(ATTRIBUT_INDEX);
		analyzer = (StandardAnalyzer) getServletContext().getAttribute(ATTRIBUT_ANALYZER);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String query = request.getParameter("recherche");
		if(query == null || query.equals("")){
			request.setAttribute(ATTR_RECHERCHE, "Veuillez entrer un critère de recherche.");
			request.getRequestDispatcher(VUE_LISTE).forward(request, response);
			return;
		}
		IndexReader reader = null;
		try{
			Query q = new MultiFieldQueryParser(new String[]{"nom","description","responsable","auteur","tuteur"}, analyzer).parse(query);
			reader = DirectoryReader.open(index);
			System.out.println("1" + reader);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs to = searcher.search(q, 15);
			ScoreDoc[] hits = to.scoreDocs; 

			request.setAttribute(ATTR_RECHERCHE,"Trouvé " + hits.length + " résultat(s).");
			System.out.println(reader);

			ArrayList<Module> listeModules = new ArrayList<Module>(); 
			for(int i=0;i<hits.length;++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				Module module = dao.read(Integer.parseInt(d.get("id")));
				if(module != null)
					listeModules.add(module);
			}

			request.setAttribute(ATTR_LISTE, listeModules);
		}catch(ParseException e){
			request.setAttribute(ATTR_RECHERCHE, "Un problème est survenu lors du traitement de votre requête.");
		}finally{
			try{
				if(reader!=null)
					reader.close();
				
				System.out.println(request);
				request.getRequestDispatcher(VUE_LISTE).forward(request, response);
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
