package com.projectl.servlets;

import static com.projectl.config.Initialisateur.ATTRIBUT_FACTORY;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projectl.beans.Module;
import com.projectl.controls.ModuleCtr;
import com.projectl.dao.DAOException;
import com.projectl.dao.DAOFactory;
import com.projectl.dao.EnseignantDAO;
import com.projectl.dao.EtudiantDAO;
import com.projectl.dao.MembreDAO;
import com.projectl.dao.ModuleDAO;

/**
 * Servlet implementation class Module
 */
@MultipartConfig
@WebServlet("/Module")
public class ModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	private ModuleDAO dao ;
	private EnseignantDAO daoEns ;
	private MembreDAO daoMembre ;
	private EtudiantDAO daoEtudiant ;
	private static String VUE_LISTE = "/WEB-INF/dialogues/listeModules.jsp";
	private static String VUE_DESC = "/WEB-INF/dialogues/descModule.jsp";
	private static String VUE_CREATION = "/WEB-INF/dialogues/creationModule.jsp";
	private static String VUE_NOUVEAU_MODULE = "Module?id=";
	private static String VUE_ACCUEIL = "/ProjetLicence/Accueil";


	private static String PARAM_MODULE = "id";
	private static String PARAM_CREATION = "create";
	
	private static final String ATTRIBUT_RESPONSABLE = "resp";
	private static String ATTRIBUT_ENSEIGNANT_MOD = "ens";
	private static String ATTRIBUT_INSCRIT_MOD = "inscrit";
	private static String ATTR_LISTE = "listeModules";
	private static String ATTR_MODULE = "module";
	private static String ATTR_RESPONSABLE = "responsable";
	private static String ATTR_AUTEUR = "auteur";
	private static String ATTR_TUTEUR = "tuteur";
	private static String ATTR_SUPPRIMER = "supprimer";
	private static String ATTR_ID = "id";
	private static String ATTR_LISTE_ENS = "listeEnseignants";

	
	
	
	public void init(){
		DAOFactory factory = (DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY);
		dao = factory.getModuleDAO() ;
		daoEns = factory.getEnseignantDAO() ;
		daoMembre = factory.getMembreDAO() ;
		daoEtudiant = factory.getEtudiantDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String mail = (String) session.getAttribute("email");
		if(request.getParameter(PARAM_MODULE)!=null){ //Si la requête contient l'identifiant du module à afficher
			try{
				Integer id = new Integer(request.getParameter(PARAM_MODULE));
				Module module = dao.read(id);
				request.setAttribute(ATTR_MODULE, module);
				request.setAttribute(ATTR_RESPONSABLE, daoMembre.read(module.getResponsable()));
				request.setAttribute(ATTR_AUTEUR, daoMembre.read(module.getAuteur()));
				request.setAttribute(ATTR_TUTEUR, daoMembre.read(module.getTuteur()));
				request.setAttribute(ATTR_LISTE_ENS, daoMembre.listEns());
				if(mail!=null && daoEtudiant.isEtudiant(mail, id))
					request.setAttribute(ATTRIBUT_INSCRIT_MOD, "true");			
				if(mail!=null && (module.getResponsable().equals(mail) || module.getAuteur().equals(mail) || module.getTuteur().equals(mail)))
					request.setAttribute(ATTRIBUT_ENSEIGNANT_MOD, "true");		
				if(mail!=null && module.getResponsable().equals(mail)){
					request.setAttribute(ATTRIBUT_RESPONSABLE, "true");
				}
				request.getSession().setAttribute("id", id);
				request.getRequestDispatcher(VUE_DESC).forward(request,response);
				return;
			}catch(NumberFormatException e){
				response.sendError(400,"Vous essayez d'accéder à un module invalide.");
				return;
			}catch(NullPointerException e){
			response.sendError(404,"Ce module n'existe pas.");
			return;
		}
		}
		
		if(new Boolean(request.getParameter(PARAM_CREATION))){ //Si la requête contient une demande de création d'un module
			if(mail!=null && daoEns.readResponsables(mail)!=null){ 
				request.getRequestDispatcher(VUE_CREATION).forward(request, response);			
			}else{
				response.sendError(403, "Vous n'avez pas l'accès de responsable pour la création d'un module.");
			}
			return;
		}
		ArrayList<Module> listeModules = dao.list();
		request.setAttribute(ATTR_LISTE,listeModules);
		request.getRequestDispatcher(VUE_LISTE).forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String mail = (String) session.getAttribute("email");
		
		if(new Boolean(request.getParameter(ATTR_SUPPRIMER))){
			try{
				Integer clefModule = new Integer(request.getParameter(ATTR_ID));
				new ModuleCtr(mail).supprimerModule(clefModule);
				response.sendRedirect(VUE_ACCUEIL);
				return ;
			}catch(NumberFormatException e){
				response.sendError(400,"Le paramètre du module est invalide.");
				return ;
			}catch(DAOException e){
				response.sendError(500,"Une erreur a eu lieu pendant la suppression.");
				e.printStackTrace();
				return ;
			}
		}
			
		
		if(mail!=null && daoEns.readResponsables(mail)!=null){
			int id = new ModuleCtr(dao, mail,request).creerModule();
			if(id > 0)
				response.sendRedirect(VUE_NOUVEAU_MODULE + id);
			else
				request.getRequestDispatcher(VUE_CREATION).forward(request, response);			
		}else{
			response.sendError(403, "Vous n'avez pas l'accès de responsable pour la création d'un module.");
		}
	}
}
