package com.projectl.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

import com.projectl.beans.Membre;
import com.projectl.dao.DAOException;
import com.projectl.dao.MembreDAO;
import com.projectl.dao.DAOFactory;

import static com.projectl.config.Initialisateur.*;

@WebServlet("/Authentification")
public class Authentification extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String PARAMETRE_EMAIL = "email";
	private static final String PARAMETRE_MDPASSE = "password";
	private static final String ALGO_CHIFFREMENT = "SHA-256";
	private static final String VUE = "WEB-INF/dialogues/login.jsp";
	private static final String ACCUEIL = "/ProjetLicence/Accueil";
	private MembreDAO dao; 

	public void init() throws ServletException {
		dao = ((DAOFactory) getServletContext().getAttribute(ATTRIBUT_FACTORY)).getMembreDAO();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("email")==null)
			request.getRequestDispatcher(VUE).forward(request, response);
		else
			response.sendRedirect(ACCUEIL);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter(PARAMETRE_EMAIL);
		String mdp = request.getParameter(PARAMETRE_MDPASSE);
		try{
			Membre membre = dao.read(email); //recherche du membre dans la bdd
			/* vérification du mot de passe */
			if(membre != null){
				ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
				passwordEncryptor.setAlgorithm(ALGO_CHIFFREMENT);
				passwordEncryptor.setPlainDigest(false);	
				if(passwordEncryptor.checkPassword(mdp, membre.getMdpasse())){
					HttpSession session = request.getSession() ;
					session.setAttribute("email", email);
					response.sendRedirect(ACCUEIL);
					return;
				}
			}
			request.setAttribute("erreur", "L'adresse ou le mot de passe est erroné.");
			request.getRequestDispatcher(VUE).forward(request, response);
		}catch(DAOException e){
			e.printStackTrace();
			request.setAttribute("erreur","Problème d'accès à la base de données, merci de réessayer.");
			request.getRequestDispatcher(VUE).forward(request, response);
		}
	}

}
