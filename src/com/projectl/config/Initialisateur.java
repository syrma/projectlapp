package com.projectl.config;

import java.io.IOException;
import java.util.ArrayList;

import com.projectl.dao.DAOFactory;
import com.projectl.dao.ModuleDAO;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class Initialisateur implements ServletContextListener {
	private DAOFactory factory ;
	public static final String ATTRIBUT_FACTORY = "daofactory"; 
	public static final String ATTRIBUT_ANALYZER = "analyzer"; 
	public static final String ATTRIBUT_INDEX = "index"; 
	private StandardAnalyzer analyzer ;
	private Directory index; 
	private IndexWriter w;

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext() ;		
		factory = DAOFactory.getInstance();		
		context.setAttribute(ATTRIBUT_FACTORY, factory);

		ModuleDAO moduleDAO = factory.getModuleDAO();	
		ArrayList<String[]> list = moduleDAO.index() ;

		analyzer = new StandardAnalyzer();
		index = new RAMDirectory();

		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		try{
			w = new IndexWriter(index, config);

			for(int i = 0 ; i < list.size() ; i++){
				Document doc = new Document();
				doc.add(new StringField("id", list.get(i)[0], Field.Store.YES));			
				doc.add(new TextField("nom", list.get(i)[1], Field.Store.NO));
				doc.add(new TextField("description", list.get(i)[2], Field.Store.NO));
				doc.add(new TextField("niveau", list.get(i)[3], Field.Store.NO));
				doc.add(new TextField("responsable", list.get(i)[3] +" " + list.get(i)[4], Field.Store.NO));
				doc.add(new TextField("auteur", list.get(i)[5] +" " + list.get(i)[6], Field.Store.NO));
				doc.add(new TextField("tuteur", list.get(i)[7] +" " + list.get(i)[8], Field.Store.NO));

				w.addDocument(doc);
			}
			w.close();
			context.setAttribute(ATTRIBUT_ANALYZER, analyzer);
			context.setAttribute(ATTRIBUT_INDEX, index);
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			

		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
/*
		try{
			

		}catch(IOException e){
			e.printStackTrace();
		}
		*/
	}


}
