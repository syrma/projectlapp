package com.forum.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.forum.beans.Message;
import com.projectl.dao.DAOFactory ;
import com.projectl.dao.DAOException ;

import static com.projectl.dao.DAOUtilitaire.* ;

public class MessageDAOImpl implements MessageDAO{
	static final String SQL_ADD = "INSERT INTO Message(clefTopic,auteurMessage,datePublication,dateModif,contenu) VALUES (?,?,?,?,?);";
	static final String SQL_READ = "SELECT * FROM Message WHERE(clefMessage = ?);";
	static final String SQL_UPDATE = "UPDATE Message SET contenu = ?, dateModif = ? WHERE clefMessage = ? ;" ;
	static final String SQL_DELETE = "DELETE FROM Message WHERE clefMessage = ? ;" ;
	DAOFactory factory ; 
	
	public MessageDAOImpl(DAOFactory factory){
		this.factory = factory ;
	}
	
	@Override
	public void add(Message message) {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false, message.getClefTopic(), message.getAuteurMessage(), message.getDatePublication(),message.getDateModif(), message.getContenu());
			int i = stmt.executeUpdate() ; 
			if(i==0)
				throw new DAOException("Le message n'a pas pu être posté. Veuillez réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}

	@Override
	public Message read(Integer clefMessage) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ; 
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false,clefMessage);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null ; 
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
		
	}

	@Override
	public void update(Integer clefMessage, Message message) {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,message.getContenu(),message.getDateModif(),message.getClefMessage());
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du message, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}

	@Override
	public void delete(Integer clefMessage) {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_DELETE,false,clefMessage);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Le message n'a pas pu être supprimé, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
		
	}
	
	private Message map(ResultSet rs){
		Message message = new Message();
		try{
			message.setClefMessage(rs.getInt("clefMessage"));
			message.setAuteurMessage(rs.getString("auteurMessage"));
			message.setClefTopic(rs.getInt("clefTopic"));
			message.setDatePublication(rs.getTimestamp("datePublication"));
			message.setDateModif(rs.getTimestamp("dateModif"));
			message.setContenu(rs.getString("contenu"));
		}catch(SQLException e){
			throw new DAOException(e);
		}
		return message ;
	}

}
