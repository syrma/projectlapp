package com.forum.dao;

import static com.projectl.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.projectl.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.forum.beans.Topic;
import com.projectl.dao.DAOException;
import com.projectl.dao.DAOFactory;

public class TopicDAOImpl implements TopicDAO {
	private static final String SQL_ADD = "INSERT INTO Topic(clefSeance,titreTopic,dateCreation) VALUES (?,?,?);";
	private static final String SQL_READ = "SELECT * FROM Topic WHERE clefTopic = ?;";
	private static final String SQL_LIST = "SELECT * FROM Topic WHERE clefSeance = ? ";
	private static final String SQL_UPDATE = "UPDATE Topic SET clefSeance = ?, titreTopic = ? WHERE clefTopic = ? ;" ;
	private static final String SQL_DELETE = "DELETE FROM Topic WHERE clefTopic = ? ;" ;
	
	DAOFactory factory ; 
	
	public TopicDAOImpl(DAOFactory factory){
		this.factory = factory ;
	}
	@Override
	public void add(Topic topic) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_ADD, false,topic.getClefSeance(), topic.getTitreTopic(), topic.getDateCreation());
			int i = stmt.executeUpdate() ; 
			if(i==0)
				throw new DAOException("Une erreur est survenue lors de la création du topic. Veuillez réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public Topic read(Integer clefTopic) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ; 
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx, SQL_READ, false,clefTopic);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null ; 
			else
				return map(rs);
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

	@Override
	public void update(Integer clefTopic, Topic topic) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_UPDATE,false,topic.getClefSeance(), topic.getTitreTopic(), clefTopic);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Un problème est survenu durant la modification du topic, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}
	}

	@Override
	public void delete(Integer clefTopic) throws DAOException {
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		try{
			cnx = factory.getConnection();
			stmt = initialisationRequetePreparee(cnx,SQL_DELETE,false,clefTopic);
			int i = stmt.executeUpdate();
			if(i==0)
				throw new DAOException("Le message n'a pas pu être supprimé, merci de réessayer.");
		}catch(SQLException e){
			throw new DAOException(e);
		}finally{
			fermeturesSilencieuses(stmt, cnx);
		}

	}
	
	private Topic map(ResultSet rs){
		Topic topic = new Topic() ;
		try{
			topic.setClefTopic(rs.getInt("clefTopic"));
			topic.setClefSeance(rs.getInt("clefSeance"));
			topic.setTitreTopic(rs.getString("titreTopic"));
			topic.setDateCreation(rs.getDate("dateCreation"));
		}catch(SQLException e){
			throw new DAOException(e);
		}
		return topic ;
	}
	@Override
	public ArrayList<Topic> list(Integer clefSeance) throws DAOException {
		ArrayList<Topic> liste = new ArrayList<Topic>();
		Connection cnx = null ;
		PreparedStatement stmt = null ;
		ResultSet rs = null ;
		try{
			cnx = factory.getConnection() ;
			stmt = initialisationRequetePreparee(cnx, SQL_LIST, false, clefSeance);
			rs = stmt.executeQuery();
			while(rs.next())
				liste.add(map(rs));
			return liste;
		}catch(SQLException e){
			throw new DAOException(e);
		}
		finally{
			fermeturesSilencieuses(rs, stmt, cnx);
		}
	}

}
