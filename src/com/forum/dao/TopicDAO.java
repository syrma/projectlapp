package com.forum.dao;

import java.util.ArrayList;

import com.forum.beans.Topic;
import com.projectl.dao.DAOException;

public interface TopicDAO {
	void add(Topic topic) throws DAOException;
	Topic read(Integer clefTopic) throws DAOException;
	void update(Integer clefTopic, Topic topic) throws DAOException;
	void delete(Integer clefTopic) throws DAOException;
	ArrayList<Topic> list(Integer clefSeance) throws DAOException ;

}
