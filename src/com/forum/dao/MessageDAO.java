package com.forum.dao;

import com.forum.beans.Message;
import com.projectl.dao.DAOException;

public interface MessageDAO {

	void add(Message message) throws DAOException;
	Message read(Integer clefMessage) throws DAOException;
	void update(Integer clefMessage, Message message) throws DAOException;
	void delete(Integer clefMessage) throws DAOException;
	
}
