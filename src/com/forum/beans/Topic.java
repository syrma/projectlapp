package com.forum.beans;

import java.io.Serializable;
import java.util.Date;
public class Topic implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Integer clefTopic, clefSeance ;
	String titreTopic ;  
	Date dateCreation ;
	
	public Integer getClefTopic() {
		return clefTopic;
	}
	public void setClefTopic(Integer clefTopic) {
		this.clefTopic = clefTopic;
	}
	public Integer getClefSeance() {
		return clefSeance;
	}
	public void setClefSeance(Integer clefSeance) {
		this.clefSeance = clefSeance;
	}
	public String getTitreTopic() {
		return titreTopic;
	}
	public void setTitreTopic(String titreTopic) {
		this.titreTopic = titreTopic;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	
}
