package com.forum.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

	private static final long serialVersionUID = 1L;

	Integer clefMessage,clefTopic ;
	String auteurMessage ;
	Date datePublication ;
	Date dateModif ;
	String contenu ;
	
	public Integer getClefMessage() {
		return clefMessage;
	}
	public void setClefMessage(Integer clefMessage) {
		this.clefMessage = clefMessage;
	}
	public Integer getClefTopic() {
		return clefTopic;
	}
	public void setClefTopic(Integer clefTopic) {
		this.clefTopic = clefTopic;
	}
	public String getAuteurMessage() {
		return auteurMessage;
	}
	public void setAuteurMessage(String auteurMessage) {
		this.auteurMessage = auteurMessage;
	}
	public Date getDatePublication() {
		return datePublication;
	}
	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}
	public Date getDateModif() {
		return dateModif;
	}
	public void setDateModif(Date dateModif) {
		this.dateModif = dateModif;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	
	
}
