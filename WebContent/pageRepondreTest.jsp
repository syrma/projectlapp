<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="WEB-INF/dialogues/header.jsp" %>

	    <%@ page import="java.util.ArrayList"%>
	    <%@ page import="java.util.Map"%>
		<%@ page import="com.projectl.beans.Test"%>
		<%@ page import="com.projectl.beans.Question"%>
		<%@ page import="com.projectl.beans.Reponse"%>
	
	<script type='text/javascript' src='/ProjetLicence/tipsy/javascripts/jquery.tipsy.js'></script>
	<link rel="stylesheet" href="/ProjetLicence/tipsy/stylesheets/tipsy.css" type="text/css" />
		
	<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min.css" media="all">
	<script src="/ProjetLicence/aui/js/aui.min.js"></script>

<% Test test = (Test) request.getAttribute("test"); %>
<div class="formulaire paintedGradient col-8">
	<h1>Test</h1>
		<% Map<Integer,Boolean> resultat = (Map<Integer,Boolean>) request.getAttribute("resultat");
			Integer note = (Integer) request.getAttribute("note"); %>
	<form class="formTest aui defaultFont" action="" method="post">
	<%if(note == null){ %>
		<div class="aui-message aui-message-error">
			<p class="title">
				<strong>ATTENTION</strong>
			</p>
			<p>Plusieurs choix peuvent être corrects.</p>
		</div>
		<%}%>
		
		<% int i = 1;
		for(Question question : test.getQuestions()){ %>
		<h2>Question <%=i++ %></h2>
		<div id="question1" class="paintedGradient">
			<h1 class="question-text"><%=question.getContenu() %></h1>
			<table id="options-table" class="aui">
				<thead>
				</thead>
				<tbody>
				<% for(Reponse reponse : question.getReponses()){%>
					<tr>
						<td><%=reponse.getContenu() %></td>
				<%if(resultat == null){ %>
						<td><input type="checkbox" name="<%=reponse.getClefReponse()%>" value="true"></td>
				<%}else{
					boolean reponseEtudCorrecte= resultat.get(reponse.getClefReponse()) ; 
					boolean boxWasChecked = (reponseEtudCorrecte == reponse.getEstCorrecte()); %>
						<td><input type="checkbox" name="<%=reponse.getClefReponse()%>" <%=boxWasChecked?"checked":""%> value="true"></td>
				<%if(reponseEtudCorrecte){ %>
		 				<td><span class="aui-icon aui-icon-small aui-iconfont-success">Correcte</span></td> 
		 		<%}else{ %>
		 				<td><span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Incorrecte</span></td>
		 		<% }} %>
					</tr>
					<%} %>
				</tbody>
			</table>
		</div>
		<%} %>
		<input type="hidden" value="<%=test.getClefTest()%>" name = "idTest">
		<% if(note == null){%>
		<input type="submit" value="Envoyer les réponses" style="padding: 10px;margin-left: auto; margin-right: auto; display: block;">
		<%}else{ %>
		<div style="padding: 10px;margin-left: auto; margin-right: auto; display: block;">Votre note : <%=note %></div>
		<%} %>
	</form>
</div>
