<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.projectl.beans.Cours"%>
<%@ page import="java.util.ArrayList"%>
	    <%@ page import="com.projectl.beans.Seance"%>


<% ArrayList<Cours> listeCours = (ArrayList<Cours>) request.getAttribute("listeCours");%>

<div class="row lessonControl dropdown">
	<button aria-owns="dwarfers" aria-haspopup="true"
		class="aui-button aui-style-default aui-dropdown2-trigger"><jsp:getProperty name="coursCourant" property="nom" /></button>
	<div id="dwarfers" class="aui-style-default aui-dropdown2">
		
		<ul class="dropdown-menu aui-list-truncate">
			<% 
if(listeCours != null){
	for(Cours cours : listeCours) {%>
	<li>
				<a id="courseName" href="/ProjetLicence/InscriptionModule?id=<%=request.getParameter("id")%>&idCours=<%=cours.getClefCours()%>"><%=cours.getNom() %></a>
				<a class="courseTouls removeToul" href="#"><span class="aui-button aui-button-subtle aui-icon aui-icon-small aui-iconfont-delete" title="Supprimer ce cours"></span></a>
				<a class="courseTouls modificationToul" href="/ProjetLicence/GererCours?idCours=<%=cours.getClefCours()%>"><span class="aui-button aui-button-subtle aui-icon aui-icon-small aui-iconfont-configure" title="Modifier ce cours"></span></a>
			</li>
			<%	}
}
%>
			<li><a
				href="/ProjetLicence/Cours?id=<%=request.getParameter("id") %>"><button
						class="aui-button aui-button-subtle defaultFont">
						<span class="aui-icon aui-icon-small aui-iconfont-add"></span>Créer
						cours
					</button></a></li>
		</ul>


		<script type='text/javascript'>
		 $(function() {
		   $('.dropdown span').tipsy({fade: true, gravity: 'n'});
		 });
		</script>



		<!-- Render the dialog -->
		<section role="dialog" id="dialog-cours" class="defaultFont aui-layer aui-dialog2 aui-dialog2-small .aui-dialog2-warning" aria-hidden="true">
			<header class="aui-dialog2-header">
				<h2 class="aui-dialog2-header-main"></h2>
				<a class="aui-dialog2-header-close"> 
				<span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
				</a>
			</header>
			<div class="aui-dialog2-content">
				<p></p>
			</div>
			<footer class="aui-dialog2-footer">
				<div class="aui-dialog2-footer-actions">
					<button id="dialog-close-button" class="aui-button aui-button-link">Close</button>
				</div>
			</footer>
		</section>



		<script type="text/javascript">
		//A dialog will appear when the modification/suppression option is clicked on.
		$('.removeToul , .modificationToul').on('click', function(e){
			 e.preventDefault();
				var nomCours = $(this).prevAll().text();
	        	var $element = $(this).clone();
	        	$element.children().remove();
	        	$element.removeClass('courseTouls');
	        	$element.addClass('aui-button aui-button-link');
	        	
	        	if($element.attr('class').indexOf('modificationToul') != -1){
	        		$('.aui-dialog2-header-main').text('Modifier un cours!');
	        		$('.aui-dialog2-content').children().text('Voullez vous modifier le cours: "'+ nomCours+'" ?');
	        		$('#dialog-cours').removeClass('aui-dialog2-warning').addClass('aui-dialog2-modification');
	        		$element.text('Modifer');
	        	}
	        	else{
	        		$('.aui-dialog2-header-main').text('Supprimer un cours!');
	        		$('.aui-dialog2-content').children().text('Voullez vous supprimer le cours: "'+ nomCours+'" ?');
	        		$('#dialog-cours').removeClass('aui-dialog2-modification').addClass('aui-dialog2-warning');
	        		$element.text('Supprimer');
	        	}
	        	$('#dialog-close-button').prev().remove();
	        	$('#dialog-close-button').before($element);
				AJS.dialog2("#dialog-cours").show();
		});

        // Hides the dialog
        AJS.$("#dialog-close-button").click(function(e) {
        e.preventDefault();
            AJS.dialog2("#dialog-cours").hide();
        });

        // Show event - this is triggered when the dialog is shown
        AJS.dialog2("#dialog-cours-suppression").on("show", function() {
            console.log("demo-dialog was shown");
        });

        // Hide event - this is triggered when the dialog is hidden
        AJS.dialog2("#dialog-cours-suppression").on("hide", function() {
            console.log("demo-dialog was hidden");
        });

        // Global show event - this is triggered when any dialog is show
        AJS.dialog2.on("show", function() {
            console.log("a dialog was shown");
        });

        // Global hide event - this is triggered when any dialog is hidden
        AJS.dialog2.on("hide", function() {
            console.log("a dialog was hidden");
        });
        </script>
	</div>


</div>