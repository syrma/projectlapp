	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
		<div class="row moduleControl">
			<h1><%=request.getAttribute("nomModule") %></h1>
			<div class="aui-buttons defaultFont">
				<button id="dialog-show-button" class="aui-button">Supprimer</button> 
				<button id="dialog-show-button 2" class="aui-button">Modifier</button>
				<a href="/ProjetLicence/Module?id=<%=request.getParameter("id")%>" class="aui-button">Description</a>
			</div>
		</div>
	
		                <!-- Render the dialog -->
                <section role="dialog" id="demo-dialog" class="aui-layer aui-dialog2 aui-dialog2-medium aui-dialog2-warning defaultFont" aria-hidden="true">
                    <!-- Dialog header -->
                    <header class="aui-dialog2-header">
                        <!-- The dialog's title -->
                        <h2 class="aui-dialog2-header-main">Attention !!</h2>
                        <!-- Actions to render on the right of the header -->
                        <!-- Close icon -->
                        <a class="aui-dialog2-header-close">
                            <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                        </a>
                    </header>
                    <!-- Main dialog content -->
                    <div class="aui-dialog2-content">
                        <p>Voulez-vous vraiment supprimer le module : "nom de module" ? Les inscriptions des étudiants, les cours du module ainsi que leurs composants seront détruits.</p>
                    </div>
                    <!-- Dialog footer -->
                    <footer class="aui-dialog2-footer">
                        <!-- Actions to render on the right of the footer -->
                        <div class="aui-dialog2-footer-actions">
                        <form action="/ProjetLicence/Module" method = "post">
                        	<input class="aui-button aui-button-link" type = "submit" value = "Supprimer">
                            <button id="dialog-close-button" class="aui-button aui-button-link">Close</button>
                            <input type="hidden" value = "<%=request.getParameter("id") %>" name = "id"/>
                            <input type="hidden" value = "true" name = "supprimer"/>
                        </form>
                        </div>
                        <!-- Hint text is rendered on the left of the footer -->
                    </footer>
                </section>
                
                
                
                		<script type="text/javascript">
        // Shows the dialog when the "Show dialog" button is clicked
        AJS.$("#dialog-show-button").click(function() {
            AJS.dialog2("#demo-dialog").show();
        });
        
        // Hides the dialog
        AJS.$("#dialog-close-button").click(function(e) {
        e.preventDefault();
            AJS.dialog2("#demo-dialog").hide();
        });

        // Show event - this is triggered when the dialog is shown
        AJS.dialog2("#demo-dialog").on("show", function() {
            console.log("demo-dialog was shown");
        });

        // Hide event - this is triggered when the dialog is hidden
        AJS.dialog2("#demo-dialog").on("hide", function() {
            console.log("demo-dialog was hidden");
        });

        // Global show event - this is triggered when any dialog is show
        AJS.dialog2.on("show", function() {
            console.log("a dialog was shown");
        });

        // Global hide event - this is triggered when any dialog is hidden
        AJS.dialog2.on("hide", function() {
            console.log("a dialog was hidden");
        });
		</script>
                
                <!-- Render the dialog -->
                <section role="dialog" id="demo-dialog 2" class="aui-layer aui-dialog2 aui-dialog2-medium aui-dialog2-warning defaultFont" aria-hidden="true">
                    <!-- Dialog header -->
                    <header class="aui-dialog2-header">
                        <!-- The dialog's title -->
                        <h2 class="aui-dialog2-header-main">Suppression!!!</h2>
                        <!-- Actions to render on the right of the header -->
                        <!-- Close icon -->
                        <a class="aui-dialog2-header-close">
                            <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                        </a>
                    </header>
                    <!-- Main dialog content -->
                    <div class="aui-dialog2-content">
                        <p>Voullez vous supprimer le module: "nom de module" ??</p>
                    </div>
                    <!-- Dialog footer -->
                    <footer class="aui-dialog2-footer">
                        <!-- Actions to render on the right of the footer -->
                        <div class="aui-dialog2-footer-actions">
                        	<button class="aui-button aui-button-link">Supprimer</button>
                            <button id="dialog-close-button" class="aui-button aui-button-link">Close</button>
                        </div>
                    </footer>
                </section>
                
                
        <script type="text/javascript">
        // Shows the dialog when the "Show dialog" button is clicked
        AJS.$("#dialog-show-button 2").click(function() {
            AJS.dialog2("#demo-dialog 2").show();
        });
        

        // Show event - this is triggered when the dialog is shown
        AJS.dialog2("#demo-dialog 2").on("show", function() {
            console.log("demo-dialog was shown");
        });

        // Hide event - this is triggered when the dialog is hidden
        AJS.dialog2("#demo-dialog 2").on("hide", function() {
            console.log("demo-dialog was hidden");
        });

        // Global show event - this is triggered when any dialog is show
        AJS.dialog2.on("show", function() {
            console.log("a dialog was shown");
        });

        // Global hide event - this is triggered when any dialog is hidden
        AJS.dialog2.on("hide", function() {
            console.log("a dialog was hidden");
        });
		</script>
		
		
		
		
