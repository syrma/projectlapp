<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
		<%@ page import="com.projectl.beans.Cours"%>
	    <%@ page import="java.util.ArrayList"%>


<% ArrayList<Cours> listeCours = (ArrayList<Cours>) request.getAttribute("listeCours");%>

<div class="row lessonControl dropdown">
	<button aria-owns="dwarfers" aria-haspopup="true"
		class="aui-button aui-style-default aui-dropdown2-trigger"> <jsp:getProperty name="coursCourant" property="nom" /> </button>

	<!-- ************************************************************************************************** -->
	<!-- *******************listeCoursAuteur.jsp OU listeCoursNormal.jsp(pour tous les autres acteurs)******** -->

	<div id="dwarfers" class="aui-style-default aui-dropdown2">
		<ul class="dropdown-menu">
			<%
if(listeCours != null){
	for(Cours cours : listeCours) {%>
			<li><a
				href="/ProjetLicence/InscriptionModule?id=<%=request.getParameter("id")%>&idCours=<%=cours.getClefCours() %>">
				<%=cours.getNom() %> </a></li>
			<%	}
}
%>
		</ul>
	</div>

	<!-- ************************************************************************************************** -->
	<!-- ************************************************************************************************** -->

</div>
