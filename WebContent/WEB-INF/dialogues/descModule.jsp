<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>
	<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min.css" media="all">
	<script src="/ProjetLicence/aui/js/aui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/button.css">
		
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="com.projectl.beans.Membre"%>
<jsp:useBean id="module" scope="request" class="com.projectl.beans.Module" />
<jsp:useBean id="responsable" scope="request" class="com.projectl.beans.Membre" />
<jsp:useBean id="auteur" scope="request" class="com.projectl.beans.Membre" />
<jsp:useBean id="tuteur" scope="request" class="com.projectl.beans.Membre" />

	<div class="grid">
		<div class="row">
			<div class="nomModule paintedGradient animated wow fadeInDown animated col-12">
				<h1 id="nomModule"><jsp:getProperty name="module" property="nom" /></h1>
			</div>
		</div>
		<%
String erreur = (String) request.getAttribute("error");
if(erreur!=null){
    	out.println(erreur);
}
%>
		<div class="row">
			<div class="descriptionModule col-8">
				<h2>Informations sur le module</h2>
								
				<div class="infoModuleSection">
					<p>Date de Creation : </p> <jsp:getProperty name="module" property="dateParution" /> 
					<p>Niveau :  </p><jsp:getProperty name="module" property="niveauLisible" />
				</div>
				<h2>Le sommaire du module</h2>
				
				<p>
				<jsp:getProperty name="module" property="description" />
				</p>
					
				<h2>Les enseignants du module</h2>
				
				<div class="instructorsSection">
					<ul class="listeInstructors">
					
						<li class="animated wow fadeInDown animated">
							<div class="imageInstructor">
								<img class="avatar" src="<jsp:getProperty name="responsable" property="avatarURL" />" height="150" width="150">
								<span class="triangle"></span>
							</div> 
							
							<div class="instructor_details">
								<h4><jsp:getProperty name="responsable" property="nomComplet" /> </h4>
								<h5>Responsable</h5>
							</div>
						</li>
						
						<li class="animated wow fadeInDown animated">
							<div class="imageInstructor">
								<img class="avatar" src="<jsp:getProperty name="auteur" property="avatarURL" />" height="150" width="150">
								<span class="triangle"></span>
							</div> 
							<div class="instructor_details">
								<h4><jsp:getProperty name="auteur" property="nomComplet" /></h4>
								<h5>Auteur</h5>
							</div>
							
							<%if(new Boolean((String)request.getAttribute("resp"))){ %>
					<button type="button" id="assign-auteur" class="aui-button defaultFont"><span class="aui-icon aui-icon-small aui-iconfont-space-personal"></span> Assigner un auteur</button>
							<% } %>
							
						</li>
						
						<li class="animated wow fadeInDown animated">
							<div class="imageInstructor">
								<img class="avatar" src="<jsp:getProperty name="tuteur" property="avatarURL" />" height="150" width="150">
								<span class="triangle"></span>
							</div> 
							<div class="instructor_details">
								<h4><jsp:getProperty name="tuteur" property="nomComplet" /></h4>
								<h5>Tuteur</h5>
							</div>
							
					<%if(new Boolean((String)request.getAttribute("resp"))){ %>
					<button type="button" id="assign-tuteur" class="aui-button defaultFont"><span class="aui-icon aui-icon-small aui-iconfont-space-personal"></span> Assigner un tuteur</button>
					<% } %>
					
						</li>
						
					</ul>
				</div>
			</div>
			<div class="moduleMaterialsSection col-3 animated wow fadeInDown animated">
					<h3>Accéder aux ressources du module:</h3>
					<img src="<jsp:getProperty name="module" property="imageURL" />" height="230" width="230"> 
					<form method ="post" action = "/ProjetLicence/InscriptionModule">
					<% if(new Boolean((String)request.getAttribute("inscrit"))){ %>
						<a href="/ProjetLicence/InscriptionModule?id=<%=request.getParameter("id")%>" class="button button-orange"> Reprendre les cours </a> 
					<%}else if(new Boolean((String)request.getAttribute("ens"))){%>
						<a href="/ProjetLicence/InscriptionModule?id=<%=request.getParameter("id")%>" class="button button-orange"> Accéder et gérer ce module </a>
						<% }else{ %>
						<input type = submit class="button button-blue" value = "S'INSCRIRE"/> 
					<%} %>
					
					<input type = "hidden" value ="<%=request.getParameter("id") %>" name = "id"/>
					<input type = "hidden" value ="true" name = "inscription"/>
					</form>
			</div>
		</div>
	</div>
	
	<!-- *********************************************************************************************************************************************************** -->
	<!-- *********************************************************************************************************************************************************** -->
	
	
	<%ArrayList<Membre> listeEns =(ArrayList<Membre>) request.getAttribute("listeEnseignants"); %>
	
	<section role="dialog" id="tuteur-dialog" class="defaultFont aui-layer aui-dialog2 aui-dialog2-medium" aria-hidden="true">
    <header class="aui-dialog2-header">
        <!-- The dialog's title -->
        <h2 class="aui-dialog2-header-main">Choisir le tuteur</h2>
        <!-- Close icon -->
        <a class="aui-dialog2-header-close">
            <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
        </a>
    </header>
    <!-- Main dialog content -->
    
    <form action="/ProjetLicence/Statut" method="post">
    <div class="aui-dialog2-content paintedGradient" style="overflow-y: auto; height: 190px !important;">
        <table class="aui">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>E-mail</th>
                    <th>Choisir</th>
                </tr>
            </thead>
            <tbody>
            <%	for(Membre ens : listeEns){ %>
            	<tr>
                	<td><%=ens.getNom() %></td>
               		<td><%=ens.getPrenom() %></td>
                    <td><%=ens.getEmail() %></td>
                    <td><input type="radio" name="enseignant" checked value="<%=ens.getEmail()%>"/></td>
                    <td></td>
                </tr>
             <% } %>
            </tbody>
        </table>
    </div>
    <!-- Dialog footer -->
    <footer class="aui-dialog2-footer">
        <!-- Actions to render on the right of the footer -->
        <div class="aui-dialog2-footer-actions">
        	<input type="submit" class="aui-button aui-button-link" value="Assigner">
            <button id="tuteur-close-button" class="aui-button aui-button-link">Close</button>
        </div>
        <!-- Hint text is rendered on the left of the footer -->
    </footer>
    <input type = "hidden" name="id" value="<%=request.getParameter("id")%>"/>
    <input type = "hidden" name="source" value="tuteur"/>
    </form>
</section>

<section role="dialog" id="auteur-dialog" class="defaultFont aui-layer aui-dialog2 aui-dialog2-medium" aria-hidden="true">
    <header class="aui-dialog2-header">
        <!-- The dialog's title -->
        <h2 class="aui-dialog2-header-main">Choisir l'auteur</h2>
        <!-- Close icon -->
        <a class="aui-dialog2-header-close">
            <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
        </a>
    </header>
    <!-- Main dialog content -->
    <form action="/ProjetLicence/Statut" method = "post">
    <div class="aui-dialog2-content paintedGradient" style="overflow-y: auto; height: 190px !important;">
    	<table class="aui">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>E-mail</th>
                    <th>Choisir</th>
                </tr>
            </thead>
            <tbody>
                  <%	for(Membre ens : listeEns){ %>
            	<tr>
                	<td><%=ens.getNom() %></td>
               		<td><%=ens.getPrenom() %></td>
                    <td><%=ens.getEmail() %></td>
                    <td><input type="radio" name="enseignant" checked value="<%=ens.getEmail()%>"/></td>
                    <td></td>
               </tr>
             <% } %>
            </tbody>
        </table>
    </div>
    <!-- Dialog footer -->
    <footer class="aui-dialog2-footer">
        <!-- Actions to render on the right of the footer -->
        <div class="aui-dialog2-footer-actions">
        	<input type="submit" class="aui-button aui-button-link" value="Assigner">
            <button id="auteur-close-button" class="aui-button aui-button-link">Close</button>
        </div>
    </footer>    
    <input type = "hidden" name="id" value="<%=request.getParameter("id")%>"/>
    <input type = "hidden" name="source" value="auteur"/>
    </form>
</section>
	
	
	
	
	
	
	<script type="text/javascript">
	// Shows the dialog when the "Show dialog" button is clicked
	AJS.$("#assign-tuteur").click(function() {
	    AJS.dialog2("#tuteur-dialog").show();
	});
	AJS.$("#assign-auteur").click(function() {
	    AJS.dialog2("#auteur-dialog").show();
	});

	// Hides the dialog
	AJS.$("#auteur-close-button").click(function(e) {
	e.preventDefault();
	    AJS.dialog2("#auteur-dialog").hide();
	});
	AJS.$("#tuteur-close-button").click(function(e) {
	e.preventDefault();
		AJS.dialog2("#tuteur-dialog").hide();
	});

	// Show event - this is triggered when the dialog is shown
	AJS.dialog2("#demo-dialog").on("show", function() {
	    console.log("demo-dialog was shown");
	});

	// Hide event - this is triggered when the dialog is hidden
	AJS.dialog2("#demo-dialog").on("hide", function() {
	    console.log("demo-dialog was hidden");
	});

	// Global show event - this is triggered when any dialog is show
	AJS.dialog2.on("show", function() {
	    console.log("a dialog was shown");
	});

	// Global hide event - this is triggered when any dialog is hidden
	AJS.dialog2.on("hide", function() {
	    console.log("a dialog was hidden");
	});
	</script>
	
	
	
	
<%@include file="footer.jsp" %>