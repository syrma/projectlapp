<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>
<jsp:useBean id="coursCourant" scope="request"
	class="com.projectl.beans.Cours" />
<% Seance seanceCourante = (Seance)request.getAttribute("seanceCourante");%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="com.projectl.beans.Cours"%>
<%@ page import="com.projectl.beans.Seance"%>
<%@ page import="com.forum.beans.Topic"%>
<%@ page import="com.projectl.beans.ContenuAdditionnel"%>

<script type='text/javascript'
	src='/ProjetLicence/tipsy/javascripts/jquery.tipsy.js'></script>
<link rel="stylesheet" href="/ProjetLicence/tipsy/stylesheets/tipsy.css"
	type="text/css" />

<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min.css"
	media="all">
<!--<link rel="stylesheet" href="/ProjetLicence/aui/css/aui-experimental.min.css" media="all"> -->
<script src="/ProjetLicence/aui/js/aui.min.js"></script>
<!--<script src="/ProjetLicence/aui/js/aui-experimental.min.js"></script>
		<!--<script src="/ProjetLicence/aui/js/aui-soy.min.js"></script> -->


</head>
<body>


	<div class="grid">

		<!-- ************************************************************************************************** -->
		<!-- ****headerResponsable.jsp OU headerEtudiant.jsp OU headerNormal.jsp(pour tous les autres acteurs)***** -->

		<%@include file="ModuleParts/headerResponsable.jsp"%>

		<!-- ************************************************************************************************** -->
		<!-- ************************************************************************************************** -->

		<div class="row">
			<!-- ************************************************************************************************** -->
			<!-- ****navigationResponsable.jsp OU navigationEtudiant.jsp OU navigationTuteur.jsp OU pasNavigation.jsp***** -->

			<%@include file="ModuleParts/navigationResponsable-Tuteur.jsp"%>

			<!-- ************************************************************************************************** -->
			<!-- ************************************************************************************************** -->

			<section class="ressourcesViewSection col-9">

				<%@include file="ModuleParts/listeCoursNormal.jsp"%>

				<% ArrayList<Seance> listeSeances = (ArrayList<Seance>) request.getAttribute("listeSeances");%>
				<div class="lessonParts">
					<h3><jsp:getProperty name="coursCourant" property="nom" />
						:
					</h3>
					<%
if(listeSeances != null){
	for(Seance seance : listeSeances) {%>
					<a
						href="/ProjetLicence/InscriptionModule?id=<%=request.getParameter("id")%>&idCours=<jsp:getProperty name="coursCourant" property="clefCours" />&idSeance=<%=seance.getClefSeance() %>"
						title="<%=seance.getNom() %>"> </a>
					<%	}
}
%>
					<script type='text/javascript'>
				 $(function() {
				   $('.lessonParts a').tipsy({fade: true, gravity: 'n'});
				 });
				</script>
				</div>


				<div class="row">
					<div class="view col-9"></div>
					<div class="viewLeft col-3">
								<h3>Contenu Additionel</h3>
								
								<%
							ArrayList<ContenuAdditionnel> listeCA = (ArrayList<ContenuAdditionnel>) request.getAttribute("listeCA");
						%>

						<ul>
							<%
								if (listeCA != null) {
									for (ContenuAdditionnel ca : listeCA) {
							%>
					<a href="/ProjetLicence/ForumServlet?id=<%=ca.getClefCA()%>" class="aui-button aui-button-primary"><%=ca.getNom()%></a>
							
							<%
								}
								}
							%>
					</div>
				</div>
				<div class="row">

					<div class="contenu col-9">

						<h3>Assistance</h3>
						<a href="#" class="aui-button aui-button-primary">Topic
							couvrant ce sujet</a>

						<%
							ArrayList<Topic> listeTopics = (ArrayList<Topic>) request
									.getAttribute("listeTopics");
						%>

						<ul>
							<%
								if (listeTopics != null) {
									for (Topic topic : listeTopics) {
							%>
							<li><a
								href="/ProjetLicence/Contenu?idca=<%=topic.getClefTopic()%>"
								title="<%=topic.getTitreTopic()%>"> </a></li>
							<%
								}
								}
							%>
						</ul>
					</div>

				</div>

				<% if(null != ((Cours) request.getAttribute("coursCourant")).getFaq()){ %>
				<div class="row">
					<div class="faq col-9">

						<h3>Frequently asked questions (FAQ)</h3>
						<dl>
							<jsp:getProperty name="coursCourant" property="faq" />
						</dl>
					</div>
				</div>
				<%} %>
			
		</div>
		</section>
	</div>
	</div>