<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>

<%@page import="java.util.List" %>
<%@page import="java.util.Calendar" %>
<%@page import="com.projectl.beans.Module" %>

	<section class="blog_posts">
		<div class="wrapper">
			<div class="title animated wow fadeIn">
				<h2>Modules</h2>
				<h3>Les formations :</h3>
				<hr class="separator"/>
			</div>

			<ul class="clearfix">
			<% List<Module> listeModules = (List<Module>) request.getAttribute("listeModules");
			for(int i = 0 ; i < listeModules.size(); i++){
				Module module = listeModules.get(i);
				Calendar cal = Calendar.getInstance() ;
				cal.setTime(module.getDateParution());
				String img = module.getImageURL() ;
				%>
				<li class="animated wow fadeInDown" data-wow-delay=".<%=i*2 %>s">
					<div class="media">
						<div class="date">
							<span class="day"><%=cal.get(Calendar.MONTH) %></span>
							<span class="month"><%=cal.get(Calendar.YEAR) %></span>
						</div>
						<a href="/ProjetLicence/Module?id=<%=module.getClefModule() %>">
							<img height = "230px" width = "230px" src="<%=img %>" alt="<%=module.getNom()%>"/>
						</a>
					</div>
					<a href="/ProjetLicence/Module?id=<%=module.getClefModule() %>">
						<h1><%=module.getNom()%>.</h1>
					</a>
				</li>
<% } %>

			</ul>
		</div>
	</section><!--  End blog_posts  -->


<%@include file="footer.jsp" %>