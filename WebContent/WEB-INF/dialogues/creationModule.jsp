<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>
<link rel="stylesheet" href="/ProjetLicence/css/style.css" />
<script type="text/javascript" src="/ProjetLicence/js/tinyeditor.js"></script>


<div class="grid">

	<div class="formulaire col-8">
	<div class="errors">
<ul>
<%@page import="java.util.ArrayList" %>

<%
ArrayList<String> erreurs = (ArrayList<String>) request.getAttribute("erreurs");
if(erreurs!=null){
	for(String erreur : erreurs) {
    	out.println("<li>" + erreur + " </li>");
	}
}
%>
</ul>
</div>
		<h1>Créer un Module</h1>
		<form  action = "/ProjetLicence/Module" method = "post"  enctype="multipart/form-data">
			<div class="field-group">
				<label for="nameModule">Le nom du module:</label>
				<input type="text" id="nameModule" name = "nom">
			</div>
			<div class="field-group">
				<label for="imageModule">Une image pour ce module:</label>
				<input type="file" id="imageModule" name = "image">
			</div>
			<div class="field-group">
				<label for="niveauModule">Choisissez le niveau de difficulté : </label>
				Basique : <input type="radio" id="imageModule" name = "niveau" value = "1" checked>
				Intermédiaire : <input type="radio" id="imageModule" name = "niveau" value = "2">
				Avancé : <input type="radio" id="imageModule" name = "niveau" value = "3">
			</div>
			
			
			<textarea id="input" style="width:400px; height:200px" name = "description"></textarea>

<script type="text/javascript">
new TINY.editor.edit('editor',{
	id:'input',
	width:584,
	height:175,
	cssclass:'te',
	controlclass:'tecontrol',
	rowclass:'teheader',
	dividerclass:'tedivider',
	controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
			  'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
			  'centeralign','rightalign','blockjustify','|','n','unformat','|','undo','redo',
			  'style','|','image','hr','link','unlink','|'],
	footer:true,
	xhtml:true,
	cssfile:'style.css',
	bodyid:'editor',
	footerclass:'tefooter',
	toggle:{text:'show source',activetext:'show wysiwyg',cssclass:'toggle'},
	resize:{cssclass:'resize'}
});
</script>

<input type = "submit" onclick="javascript:editor.post()" value= "Créer un module">

		</form>
	</div>
</div>

<%@include file="footer.jsp" %>