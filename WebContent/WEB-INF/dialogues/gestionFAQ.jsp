<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>
<link rel="stylesheet" href="/ProjetLicence/css/style.css" />
<script type="text/javascript" src="/ProjetLicence/js/tinyeditor.js"></script>


<div class="grid">
	<div class="formulaire col-8">

		<h1>Gérer la FAQ</h1>
	<div class="errors">

<%
String erreur = (String) request.getAttribute("erreur");
if(erreur!=null){
    	out.println(erreur);
}
%>
</div>
		<form  action = "/ProjetLicence/FAQ" method = "post">
<% String oldFaq = (String)request.getAttribute("faq"); 
	if(oldFaq == null) 
		oldFaq = "" ; %>
			<textarea id="input" style="width:400px; height:200px" name = "faq"><%=oldFaq %></textarea>

<script type="text/javascript">
new TINY.editor.edit('editor',{
	id:'input',
	width:584,
	height:175,
	cssclass:'te',
	controlclass:'tecontrol',
	rowclass:'teheader',
	dividerclass:'tedivider',
	controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
			  'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
			  'centeralign','rightalign','blockjustify','|','n','unformat','|','undo','redo',
			  'style','|','image','hr','link','unlink','|'],
	footer:true,
	xhtml:true,
	cssfile:'style.css',
	bodyid:'editor',
	footerclass:'tefooter',
	toggle:{text:'show source',activetext:'show wysiwyg',cssclass:'toggle'},
	resize:{cssclass:'resize'}
});
</script>
<input type="hidden" name="idCours" value="<%=request.getParameter("idCours")%>">
<input type="hidden" name="id" value="<%=request.getParameter("id")%>">

<input type = "submit" onclick="javascript:editor.post()" value= "Modifier la FAQ">

		</form>
	</div>
</div>

<%@include file="footer.jsp" %>