<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<form id="signupPage" method="POST" action = "/ProjetLicence/Inscription">

<div class="errors">
<ul>
<%@page import="java.util.ArrayList" %>

<%
ArrayList<String> erreurs = (ArrayList<String>) request.getAttribute("listeErreurs");
if(erreurs!=null){
	for(String erreur : erreurs) {
    	out.println("<li>" + erreur + " </li>");
	}
}
%>
</ul>
</div>
		<fieldset class = grid id="signupbody">
		  <fieldset>
		    <label for="nom">Nom</label>
		    <input type="text" name="nom" id="signupnom" />
		  </fieldset>
		    <fieldset>
		    <label for="pnom">Prénom</label>
		    <input type="text" name="pnom" id="signuppnom" />
		  </fieldset>
		  <fieldset>
		    <label for="email">Adresse e-mail</label>
		    <input type="text" name="email" id="signupemail" />
		  </fieldset>
		  <fieldset>
		    <label for="password">Choisir un mot de passe</label>
		    <input type="password" name="password" id="signuppassword" />
		  </fieldset>
		  <fieldset>
		    <label for="password">Confirmer le mot de passe</label>
		    <input type="password" name="confpassword" id="signuppassword1" />
		  </fieldset>
		  		  </div>
		  <input type="submit" id="signup" value="S'inscrire" />
		</fieldset>
		<div class="result">
		<%
		String result = "";
		if(request.getAttribute("resultat") !=null)
		result = (String) request.getAttribute("resultat");
		out.println(result);%>
		</div>
	      </form>
<%@include file="footer.jsp" %>