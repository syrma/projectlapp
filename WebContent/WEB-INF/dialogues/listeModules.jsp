<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>

<div class="grid">
<div class="result">
		<%
		String result = "";
		if(request.getAttribute("resultatRecherche") !=null)
		result = (String) request.getAttribute("resultatRecherche");
		out.println(result);%>
		</div>
	<%@page import="java.util.ArrayList"%>
	<%@page import="com.projectl.beans.Module"%>

	<%
		ArrayList<Module> liste = (ArrayList<Module>) request.getAttribute("listeModules");
		Module module;
		if (liste != null) {
			String img;
			for (int i = 0; i < liste.size(); i++) {
				img = liste.get(i).getImageURL();
	%>

	<div class="row">
		<div class="col-3,img">
			<a href="/ProjetLicence/Module?id=<%=liste.get(i).getClefModule()%>">
				<img height="230px" width="230px" src=<%=img%> alt="nom">
			</a>
		</div>
		<div class="col-9">
			<a href="/ProjetLicence/Module?id=<%=liste.get(i).getClefModule()%>">
				<h2><%=liste.get(i).getNom()%></h2>
			</a>
			<div class="description">
				<p><%=liste.get(i).getDescription()%></p>
			</div>

		</div>
	</div>
	<%
		}
		}
	%>


</div>


<%@include file="footer.jsp"%>