<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@include file="header.jsp" %>
<link rel="stylesheet" href="/ProjetLicence/css/style.css" />
<script type="text/javascript" src="/ProjetLicence/js/tinyeditor.js"></script>


<div class="grid">

	<div class="formulaire col-8">
	<div class="errors">

<%
String erreur = (String) request.getAttribute("erreur");
if(erreur!=null){
    	out.println(erreur);
}
%>
</div>
		<h1>Créer un cours</h1>
		<form  id="form-creation-cours" action = "/ProjetLicence/Cours" method = "post">
			<div class="field-group">
				<label for="nom">Veuillez entrer le nom du cours</label>
				<input type="text" id="nameModule" name = "nom">
					<input type = "hidden" value ="<%=request.getParameter("id") %>" name = "id"/>
			</div>
			<div class="field-group">
				<label for="nom">Entrez un indice pour le cours (n'entrez rien pour placer le cours en dernière position)</label>
				<input type="text" id="nameModule" name = "indice">
			</div>

<input type = "submit" value= "Créer un cours">

		</form>
	</div>
</div>

<%@include file="footer.jsp" %>