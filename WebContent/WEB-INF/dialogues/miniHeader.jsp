<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Project L</title>
	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="description" content="Sublime Stunning free HTML5/CSS3 website template"/>
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox-thumbs.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox-buttons.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/animate.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/main.css">
	
	<script type="text/javascript" src="/ProjetLicence/js/jquery.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-buttons.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-media.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-thumbs.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/wow.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/main.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/login.js"></script>
    
</head>
<body>

  <section class="miniBillboard billboard light">
    <header class="wrapper light">
      
   <!-- LOGO

  <a href="#"><img class="logo" src="img/logo_light.png" alt=""/></a> -->
     <nav>
<ul>
  <li><a href="">About Us</a></li>
  <li><a href="">Products</a></li>
  <li><a href="">Journal</a></li>
  <li><a href="">Contact Us</a></li>
  <% if(session.getAttribute("email") != null){%>
  <li><a href=""><%=session.getAttribute("email") %></a></li>
  <li><a href="/ProjetLicence/Deconnexion">Se déconnecter</a></li>
  <%}else{ %>
  <li id="signupContainer"><a class="signup" id="signupButton" href="#"><span>S'inscrire</span></a>
    <div id="signupBox">                
      <form id="signupForm" method="POST" action="/ProjetLicence/Inscription">
	<fieldset id="signupbody">
	  <fieldset>
	    <label for="nom">Nom</label>
	    <input type="text" name="nom" id="signupnom" />
	  </fieldset>
	    <fieldset>
	    <label for="pnom">Prénom</label>
	    <input type="text" name="pnom" id="signuppnom" />
	  </fieldset>
	  <fieldset>
	    <label for="email">Adresse e-mail</label>
	    <input type="text" name="email" id="signupemail" />
	  </fieldset>
	  <fieldset>
	    <label for="password">Choisir un mot de passe</label>
	    <input type="password" name="password" id="signuppassword" />
	  </fieldset>
	  <fieldset>
	    <label for="password">Confirmer le mot de passe</label>
	    <input type="password" name="confpassword" id="signuppassword1" />
	  </fieldset>
	  		  
	  <input type="submit" id="signup" value="S'inscrire" />
	</fieldset>
      </form>
    </div>
    <!-- Login Ends Here -->
  </li>
  <li id="loginContainer"><a class="login" id="loginButton" href="#"><span>S'authentifier</span></a>
    <div id="loginBox">                
      <form id="loginForm" action ="/ProjetLicence/Authentification" method = "POST">
	<fieldset id="body">
	  <fieldset>
	    <label for="email">Adresse e-mail</label>
	    <input type="text" name="email" id="email" />
	  </fieldset>
	  <fieldset>
	    <label for="password">Mot de passe</label>
	    <input type="password" name="password" id="password" />
	  </fieldset>
	  <!-- 
	  <label class="remeber" for="checkbox"><input type="checkbox" id="checkbox" />Se souvenir de moi</label>
	   -->
	  <input type="submit" id="login" value="login" />
	</fieldset>
      </form>
    </div>
    <!-- Login Ends Here -->
  </li>
     <% }%>
     </ul>
     </nav>
	</header>
	<div class="shadow"></div>
</section><!--  End billboard  -->
