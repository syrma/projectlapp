<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>
<link rel="stylesheet" href="/ProjetLicence/css/style.css" />

<link rel="stylesheet" href="css/jquery-ui.css" type="text/css"
	media="all" />
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>

<script>
	$(function() {
		$.datepicker.setDefaults($.extend($.datepicker.regional['fr']));
		$('#datepicker').datepicker();
	});
</script>

<div class="grid">
	<%@page import="java.util.ArrayList"%>


	<div class="formulaire col-8">
		<ul>
			<%
				ArrayList<String> erreurs = (ArrayList<String>) request
						.getAttribute("listeErreurs");
				if (erreurs != null) {
					for (String erreur : erreurs) {
						out.println("<li>" + erreur + " </li>");
					}
				}
			%>
		</ul>
		<h1>Modifer votre profil</h1>

		<form class="modificationForm" action="/ProjetLicence/Modification"
			method="post">

			<div>
				<label for="nom">Nom : </label> <input type="text" id="nom"
					name="nom">
			</div>
			<div>
				<label for="prenom">Prénom : </label> <input type="text" id="prenom"
					name="pnom">
			</div>

			<div>
				<label for="password">Entrez votre nouveau mot de passe :</label> <input
					type="password" id="password" name="password">
			</div>
			<div>
				<label for="confirmPassword">Confirmez votre nouveau mot de
					passe : </label> <input type="password" id="confirmPassword"
					name="confpassword">
			</div>
			<div>
				<label for="naissance">Entrez votre date de naissance : </label> <input
					type="date" id="datepicker" value="2015-05-06" name="dateNaissance">
			</div>
			<div>
				<label for="sexe">Sexe : </label> <select id="sexe" name="sexe">
					<option selected value="">Sélectionnez...</option>
					<option value="m" label="Homme">Homme</option>
					<option value="f" label="Femme">Femme</option>
				</select>
			</div>
			<div>
				<label for="password">Entrez votre mot de passe actuel :</label> <input
					type="password" id="password" name="oldpassword">
			</div>
			<input type="submit" value="Appliquer les modifications"
				style="margin-left: auto; margin-right: auto; display: block;">
			<div class="result">

				<%
					String result = "";
					if (request.getAttribute("resultat") != null)
						result = (String) request.getAttribute("resultat");
					out.println(result);
				%>
			</div>
		</form>
	</div>
</div>

<%@include file="footer.jsp"%>