<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Project L</title>
	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="description" content="Sublime Stunning free HTML5/CSS3 website template"/>
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox-thumbs.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox-buttons.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/animate.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/main.css">
	
	<script type="text/javascript" src="/ProjetLicence/js/jquery.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-buttons.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-media.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-thumbs.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/wow.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/main.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/login.js"></script>
    
    
    
	    
		<script type='text/javascript' src='/ProjetLicence/tipsy/javascripts/jquery.tipsy.js'></script>
		<link rel="stylesheet" href="/ProjetLicence/tipsy/stylesheets/tipsy.css" type="text/css" />	

		<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min.css" media="all">
		<!--<link rel="stylesheet" href="/ProjetLicence/aui/css/aui-experimental.min.css" media="all"> -->
		<script src="/ProjetLicence/aui/js/aui.min.js"></script>
		<!--<script src="/ProjetLicence/aui/js/aui-experimental.min.js"></script>
		<!--<script src="/ProjetLicence/aui/js/aui-soy.min.js"></script> -->
		
	
</head>
<body>
 
<!--  <section class="billboard light">
<!--    <header class="wrapper light">
<!--      
<!--   <!-- LOGO
<!--
<!--   <a href="#"><img class="logo" src="img/logo_light.png" alt=""/></a> -->
<!--      <nav>
<!--	<ul>
<!--	  <li><a href="">About Us</a></li>
<!--	  <li><a href="">Products</a></li>
<!--	  <li><a href="">Journal</a></li>
<!--	  <li><a href="">Contact Us</a></li>
<!--	  <li><a href="/ProjetLicence/Deconnexion">Se dÃ©connecter</a></li>
<!--	  <li id="signupContainer"><a class="signup" id="signupButton" href="#"><span>S'inscrire</span></a>
<!--	    <div id="signupBox">                
<!--	      <form id="signupForm" method="POST" action="/ProjetLicence/Inscription">
<!--		<fieldset id="signupbody">
<!--		  <fieldset>
<!--		    <label for="nom">Nom</label>
<!--		    <input type="text" name="nom" id="signupnom" />
<!--		  </fieldset>
<!--		    <fieldset>
<!--		    <label for="pnom">PrÃ©nom</label>
<!--		    <input type="text" name="pnom" id="signuppnom" />
<!--		  </fieldset>
<!--		  <fieldset>
<!--		    <label for="email">Adresse e-mail</label>
<!--		    <input type="text" name="email" id="signupemail" />
<!--		  </fieldset>
<!--		  <fieldset>
<!--		    <label for="password">Choisir un mot de passe</label>
<!--		    <input type="password" name="password" id="signuppassword" />
<!--		  </fieldset>
<!--		  <fieldset>
<!--		    <label for="password">Confirmer le mot de passe</label>
<!--		    <input type="password" name="confpassword" id="signuppassword1" />
<!--		  </fieldset>
<!--		  		  
<!--		  <input type="submit" id="signup" value="S'inscrire" />
<!--		</fieldset>
<!--	      </form>
<!--	    </div>
<!--	    <!-- Login Ends Here -->
<!--	  </li>
<!--	  <li id="loginContainer"><a class="login" id="loginButton" href="#"><span>S'authentifier</span></a>
<!--	    <div id="loginBox">                
<!--	      <form id="loginForm" action ="/ProjetLicence/Authentification" method = "POST">
<!--		<fieldset id="body">
<!--		  <fieldset>
<!--		    <label for="email">Adresse e-mail</label>
<!--		    <input type="text" name="email" id="email" />
<!--		  </fieldset>
<!--		  <fieldset>
<!--		    <label for="password">Mot de passe</label>
<!--		    <input type="password" name="password" id="password" />
<!--		  </fieldset>
<!--		  <!-- 
<!--		  <label class="remeber" for="checkbox"><input type="checkbox" id="checkbox" />Se souvenir de moi</label>
<!--		   -->
<!--		  <input type="submit" id="login" value="login" />
<!--		</fieldset>
<!--	      </form>
<!--	    </div>
<!--	    <!-- Login Ends Here -->
<!--	  </li>
<!--      </ul>
<!--      </nav>
<!--		</header>
<!--	
<!--		<div class="caption light animated wow fadeInDown clearfix">
<!--		<h1>Project L</h1> 
<!--			<p>Plate-forme de formation en ligne</p>
<!--			<hr>
<!--		</div>
<!--		<div class="shadow"></div>
<!--	
<!--	</section><!--  End billboard  -->


	<div class="grid">
	
		<!-- ************************************************************************************************** -->
		<!-- ****headerResponsable.jsp OU headerEtudiant.jsp OU headerNormal.jsp(pour tous les autres acteurs)***** -->
		
		<%@include file="ModuleParts/headerResponsable.jsp" %>
		
		<!-- ************************************************************************************************** -->
		<!-- ************************************************************************************************** -->
		
		<div class="row">
			<!-- ************************************************************************************************** -->
			<!-- ****navigationResponsable.jsp OU navigationEtudiant.jsp OU navigationTuteur.jsp OU pasNavigation.jsp***** -->
		
			<%@include file="ModuleParts/navigationResponsable.jsp" %>
		
			<!-- ************************************************************************************************** -->
			<!-- ************************************************************************************************** -->			<section class="ressourcesViewSection col-9">
				<div class="row lessonControl dropdown">
					<button aria-owns="dwarfers" aria-haspopup="true" class="aui-button aui-style-default aui-dropdown2-trigger">Lesson 1</button>
					
					<!-- ************************************************************************************************** -->
					<!-- *******************listeCoursAuteur.jsp OU listeCoursNormal.jsp(pour tous les autres acteurs)******** -->
					<div id="dwarfers" class="aui-style-default aui-dropdown2">
					<%@include file="ModuleParts/listeCoursAuteur.jsp" %>
					</div>
					<!-- ************************************************************************************************** -->
					<!-- ************************************************************************************************** -->
					
				</div>
				<div class="lessonParts">
				<h3>Lesson:</h3>
				      <a href="#" title="sÃ©ance 1"></a>
				      <a href="#" title="sÃ©ance 2"></a>
				      <a href="#" title="sÃ©ance 3"></a>
				      <a href="#" title="sÃ©ance 4"></a>
				      <a href="#" title="sÃ©ance 5"></a>
				      <a href="#" title="sÃ©ance 7"></a>
				      <a href="#" title="sÃ©ance 5"></a>
				      <a href="#" title="sÃ©ance 2"></a>
				      <a href="#" title="sÃ©ance 9"></a>
				      <a href="#" title="sÃ©ance 4"></a>
				      <a href="#" title="sÃ©ance 1"></a>
				      <a href="#" title="sÃ©ance 1"></a>
				      <a href="#" title="sÃ©ance 1"></a>
				      <a href="#" title="sÃ©ance 1"></a>
				      <a href="#" title="sÃ©ance 1"></a>
				      <a href="#" title="sÃ©ance 1"></a>
				      
				<script type='text/javascript'>
				 $(function() {
				   $('.lessonParts a').tipsy({fade: true, gravity: 'n'});
				 });
				</script>
				</div>


				<div class="row">
					<div class="col-9">
					<div class="row">
						<div class="view col-9"></div>
						<div class="viewLeft col-3">
						<h3>Assistance</h3>
						<a href="#" class="aui-button aui-button-primary">Topic couvrant ce sujet</a>
					</div></div>
						<div class="row">
							<div class="notes col-8">
								<h3>Notes de l'auteur</h3>
								<ul>
									<li>XML element</li>
									<li>Tag</li>
									<li>Self-closing tags</li>
									<li>Attributes</li>
									<li>Syntax</li>
								</ul>
								<p>
									You can look up their definitions <a
										href="https://developers.google.com/android/for-all/vocab-words/"
										target="_blank">in the Vocab Glossary</a>.
								</p>
								<p>Believe it or not, professional developers don't memorize
									everything -- looking up information is a key part of the job!</p>
							</div>
							<div class="contenu col-4">
								<h3>Contenu Additionelle</h3>
								<a href="#" class="aui-button aui-button-primary">fichier 1</a>
								<a href="#" class="aui-button aui-button-primary">fichier 2</a>
								<a href="#" class="aui-button aui-button-primary">fichier 3</a>
							</div>
						</div>
						<div class="row">
						<div class="faq col-9">
						<h3>Frequently asked questions (FAQ)</h3>
						<dl>
  							<dt>Are you using frames?</dt>
 								<dd>It is also important to remember that, when using frames, the bookmark function in the user's end might only register the top page. When a user then returns, he or she will have to search through the entire site again. This might cause alot of people to never visit your page more than once.</dd>
  							<dt>Did you also remember a useful ALT text for the counter?</dt>
  								<dd>If the predictions come true, 35 million users will have access to the WWW at the turn of the century. That leaves us with 3,2 million users. None of these users will be able to see the images you've added to your code</dd>
							</dl>

						
						<p> </p>
						</div>
					</div>
					
				</div>
			</section>
		</div>
	</div>
