<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>


	
	<script type='text/javascript' src='/ProjetLicence/tipsy/javascripts/jquery.tipsy.js'></script>
	<link rel="stylesheet" href="/ProjetLicence/tipsy/stylesheets/tipsy.css" type="text/css" />
		
	<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min.css" media="all">
	<script src="/ProjetLicence/aui/js/aui.min.js"></script>
    

	<div class="formulaire paintedGradient col-8">
		<h1>Créer un test final</h1>
		<form class="formTest aui defaultFont" action="/ProjetLicence/PosterInterrogation?id=<%=request.getParameter("id")%>" method="post">
			<button type="button" id="question-maker" class="aui-button defaultFont"><span class="aui-icon aui-icon-small aui-iconfont-list-add"></span> Insérer un question</button>
		
			<div id="question1" class="paintedGradient last">
				<header class="qestion-header">
					<h3>Question 1</h3>
					<div class="question-commandes aui-buttons">
						<button type="button" id="reponse-maker1" class="aui-button defaultFont"><span class="aui-icon aui-icon-small aui-iconfont-add-comment"></span> Insérer une réponse</button>
					</div>
				</header>
				<div class="questionZone field-group">
        			<label for="question1">Le question</label>	
                	<textarea class="textarea" name="question1" id="question1" placeholder="Votre question là..."></textarea>
    			</div>
				<div class="reponses">
					<div class="reponse1-1 lastReponse field-group">
                		<label for="reponse1-1">Réponse 1</label>
                		<textarea class="textarea" name="reponse1-1" id="reponse1-1" placeholder="Votre réponse là..."></textarea>
                		<input type="checkbox" name="valeur-reponse1-1" value="true">
                		<span>correcte?</span>
            		</div>
            	</div>
			</div>
			
				<input type="hidden" name = "typeInterrogation" value="test">
		<input type="hidden" name = "id" value="<%=request.getParameter("id")%>">
		<input type="hidden" name = "idCours" value="<%=request.getParameter("idCours")%>">
			<input type="submit" value="Poster le test" style="padding: 10px;margin-left: auto; margin-right: auto; display: block;">
		</form>
	</div>
	<script type="text/javascript">
	
		//A copy of a typic question.
		var $q = $('<div id="question1" class="paintedGradient last"><header class="qestion-header"><h3>Question 1</h3><div class="question-commandes aui-buttons"><button type="button" id="reponse-maker1" class="aui-button defaultFont"><span class="aui-icon aui-icon-small aui-iconfont-add-comment"></span> Insérer une réponse</button><button id="question-remover" class="aui-button defaultFont" title="Supprimer ce question"><span class="aui-icon aui-icon-small aui-iconfont-close-dialog"></span></button></div></header><div class="questionZone field-group"><label for="question1">Le question</label><textarea class="textarea" name="question1" id="question1" placeholder="Votre question là..."></textarea></div><div class="reponses"><div class="reponse1-1 lastReponse field-group"><label for="reponse1-1">Réponse 1</label><textarea class="textarea" name="reponse1-1" id="reponse1-1" placeholder="Votre réponse là..."></textarea><input type="checkbox" name="valeur-reponse1-1" value="true"><span>correcte?</span></div></div></div>');
	
		var reponseNumber = 1;
		var choixNumber = 1;
		
		$('#reponse-maker1').on('click', function(){
			$('#question1 .lastReponse').after($('<div class="reponse1-'+(++reponseNumber)+' lastReponse field-group"><label for="reponse1-'+reponseNumber+'">Réponse '+reponseNumber+'</label><textarea class="textarea" name="reponse1-'+reponseNumber+'" id="reponse1-'+reponseNumber+'" placeholder="Votre ('+reponseNumber+') réponse là..."></textarea><input type="checkbox" name="valeur-reponse1-'+reponseNumber+'" value="true"><span>correcte?</span><button type="button" id="reponse1-'+reponseNumber+'-remover" class="remover defaultFont aui-button aui-icon aui-icon-small aui-iconfont-close-dialog"></button></div>'));
			$('#question1 .lastReponse').prev().removeClass("lastReponse");
			$('#question1 .lastReponse button').on('click', function(){
				if($(this).parent().attr('class').indexOf('lastReponse') != -1)
					$('#question1 .lastReponse').prev().addClass('lastReponse');
				$(this).parent().remove();
			});
		});
		
		
	
		//this variable is useful for assigning a number to each question.
		var questionNumber = 1;
		
		$('#question-maker').on('click',function(){
			
			//Cloning the first question
			var $newQ = $q.clone(true);
			var thisQNumber = ++questionNumber;
			var reponseNumber = 1; 
			var choixNumber = 1;
			
			$newElement = $('<button type="button" id="question-remover'+thisQNumber+'" class="aui-button defaultFont" title="Supprimer ce question"><span class="aui-icon aui-icon-small aui-iconfont-close-dialog"></span></button>');
			//$newElement.tipsy({fade: true, gravity: $.fn.tipsy.autoNS});
			$newQ.find('.question-commandes button[id^="option-maker"]').after($newElement);
						
			
			$newQ.attr('id', 'question'+ (thisQNumber));
			
			$elementsToChange = $newQ.find('h3');
			$elementsToChange.text('Question '+ thisQNumber);
			
			$elementsToChange = $newQ.find('.question-commandes button[id^="reponse-maker"]');
			$elementsToChange.attr('id', 'reponse-maker'+thisQNumber);
			$elementsToChange = $newQ.find('.question-commandes button[id^="option-maker"]');
			$elementsToChange.attr('id', 'option-maker'+thisQNumber);
			//question zone
			$elementsToChange = $newQ.find('.questionZone label[for^="question"]');
			$elementsToChange.attr('for', 'question'+thisQNumber);
			
			$elementsToChange = $newQ.find('.questionZone textarea[id^="question"]');
			$elementsToChange.attr('id', 'question'+ thisQNumber);
			$elementsToChange.attr('name', 'question'+ thisQNumber);
			//response zone
			$elementsToChange = $newQ.find('.reponses .reponse1-1');
			$elementsToChange.removeClass('reponse1-1').addClass('reponse'+thisQNumber+'-1');
			
			$elementsToChange = $newQ.find('.reponses label[for^="reponse"]');
			$elementsToChange.attr('for', 'reponse'+thisQNumber+'-1');
			
			$elementsToChange = $newQ.find('.reponses textarea[id^="reponse"]');
			$elementsToChange.attr('id', 'reponse'+ thisQNumber+'-1');
			$elementsToChange.attr('name', 'reponse'+ thisQNumber+'-1');
			
			
			$elementsToChange = $newQ.find('.reponses input[type="checkbox"]');
			$elementsToChange.attr('name', 'valeur-reponse'+ thisQNumber+'-1');
			
			
			$elementsToChange = $newQ.find('.reponses button[id^="reponse"]');
			$elementsToChange.attr('id', 'reponse'+thisQNumber+'-1-remover');
			$elementsToChange.attr('id', 'reponse'+thisQNumber+'-1-remover');
			//iserting new question div after the last question in the page.
			
			var listener = $newQ.find('button[id^="question-remover"]');
			
			listener.on('click', function(){
				if($newQ.attr('class').indexOf('last') != -1)
					$newQ.prev().addClass('last');
				$newQ.remove();
			});
			
			
			$newQ.find('button[id^="reponse-maker"]').on('click', function(){
				$newQ.find('.lastReponse').after($('<div class="reponse'+thisQNumber+'-'+(++reponseNumber)+' lastReponse field-group"><label for="reponse'+thisQNumber+'-'+reponseNumber+'">Réponse '+reponseNumber+'</label><textarea class="textarea" name="reponse'+thisQNumber+'-'+reponseNumber+'" id="reponse'+thisQNumber+'-'+reponseNumber+'" placeholder="Votre ('+reponseNumber+') réponse là..."></textarea><input type="checkbox" name="valeur-reponse'+thisQNumber+'-'+reponseNumber+'" value="true"><span>correcte?</span><button type="button" id="reponse'+thisQNumber+'-'+reponseNumber+'-remover" class="remover defaultFont aui-button aui-icon aui-icon-small aui-iconfont-close-dialog"></button></div>'));
				$newQ.find('.lastReponse').prev().removeClass("lastReponse");
				$newQ.find('.lastReponse button').on('click', function(){
					if($(this).parent().attr('class').indexOf('lastReponse') != -1)
						$(this).parent().prev().addClass('lastReponse');
					$(this).parent().remove();
				});
			});
			
			
			$('.last').after($newQ);
			$('.last').removeClass('last');
			$newQ.addClass('last');
			
		});
		
		
		
	</script>
