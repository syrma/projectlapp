<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
	    <jsp:useBean id="coursCourant" scope="request" class="com.projectl.beans.Cours" />

	
	<script type='text/javascript' src='/ProjetLicence/tipsy/javascripts/jquery.tipsy.js'></script>
	<link rel="stylesheet" href="/ProjetLicence/tipsy/stylesheets/tipsy.css" type="text/css" />
		
	<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min.css" media="all">
	<script src="/ProjetLicence/aui/js/aui.min.js"></script>

	
	<div class="formulaire paintedGradient col-8">
		<div class="errors">
<ul>
<%@page import="java.util.ArrayList" %>
<%@page import="com.projectl.beans.Seance" %>


<%
ArrayList<String> erreurs = (ArrayList<String>) request.getAttribute("erreurs");
if(erreurs!=null){
	for(String erreur : erreurs) {
    	out.println("<li>" + erreur + " </li>");
	}
}
%>
</ul>
</div>
		<h1>Modifier le cours</h1>
		<div class="coursComposants aui defaultFont" >
			<h2>Publier un contenu additionnel</h2>
			<div id="publierContenu" class="paintedGradient">
			
				<form class="contenuOptions">
				<fieldset class="group">
					<legend><span>Choisir le type du contenu à ajouter : </span></legend>
						<label for="c1"> <input type="radio" checked name="contenuType" id="c1" value="seance"> Séance</label>
						<label for="c2"> <input type="radio" name="contenuType" id="c2" value="contenu"> Contenu Additionnel</label>
						<label for="c3"> <a href="/ProjetLicence/PosterInterrogation"> Poster un Quiz </a></label>
				</fieldset>
				</form>

				<form action="/ProjetLicence/Seance" class="seanceOptions" method="post" enctype="multipart/form-data">
				<fieldset class="group">
					<legend><span>Choisir le type de la séance :</span></legend>
						<label for="s1"> <input type="radio" checked name="seanceType" id="s1" value="presentation"> Présentation/Document (.pdf)</label>
						<label for="s2"> <input type="radio" name="seanceType" id="s2" value="video"> Vidéo</label>
				</fieldset>	
				<fieldset class="group">
				<legend><span>Choisir le nom et l'indice de la séance </span></legend>
				
						<label for="s3">Nom  <input type="text" name="nom" id="s3" value=""> </label>
						<label for="s4">Indice <input type="text" name="indice" id="s4" value=""> </label>
				</fieldset>	
				<fieldset class="group">
					<legend id="lastLegend"><span>Choisir le fichier à charger dans ce cours : </span></legend>
					<input type="file" id="file" name="fichier" />
					<input type="hidden" name = "idCours" value = "<%=request.getParameter("idCours")%>"/>
					</fieldset>
								
				<input type="submit" value="Publier contenu" style="padding: 10px; display: block;float: right;margin-top: 0px;">
				</form>
				
			<%ArrayList<Seance> listeSeances = (ArrayList<Seance>) request.getAttribute("listeSeances");
		if(listeSeances != null && listeSeances.size()!=0){ %>
				<form class="caOptions" action="/ProjetLicence/Contenu" enctype="multipart/form-data" method="post">
			<fieldset class="group">
			<legend><span>Choisir la séance auquel ce contenu est à rattacher : </span></legend>
			<table class="aui">
            <thead>
                <tr>
                	<th> Indice de la séance </th>
                    <th>Nom de la séance</th>
                    <th> Séléctionner</th>
                </tr>
            </thead>
            <tbody>
		<%					
			for(Seance seance : listeSeances) {%>
                <tr>
                    <td><%= seance.getIndice() %>
                    <td><%=seance.getNom()%></td>
                    <td><input type="radio" name="idSeance" value="<%=seance.getClefSeance()%>"/></td>
                </tr>
              <%   } %>
              	
            </tbody>
        </table>
					
				</fieldset>
				
				<fieldset class="group">
					<legend id="lastLegend"><span>Choisir le fichier à charger dans ce cours : </span></legend>
					<input type="file" id="file" name="fichier" />
				</fieldset>				
				<input type="submit" value="Publier contenu" style="padding: 10px; display: block;float: right;margin-top: 0px;">
						
				</form>
								
             <% } else{ %>
             <form class="caOptions">
             <fieldset class = "group">
             <legend> Il n'y a pas de séance dans ce cours pour y créer du contenu additionnel. </legend>
			</fieldset>
			<% } %>
			</form>
			</div>


			<h2>Supprimer contenu</h2>
			<div id="supprimerContenu" class="paintedGradient">
			
				<fieldset class="group">
					<legend><span>Séléctionner les contenus additionnels à supprimer:</span></legend>
					<form action="" method="post">
					<table class="aui">
            <thead>
                <tr>
                    <th>Nom du contenu additionnel</th>
                    <th >Séléctionner</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="#" class="aui-button aui-button-primary">fichier 1</a></td>
                    <td><input type="checkbox" name="contenu1" value="URL de contenu additionnel"/></td>
                    <td>
                </tr>
                <tr>
                    <td><a href="#" class="aui-button aui-button-primary">fichier 2</a></td>
                    <td><input type="checkbox" name="contenu2" value="URL de contenu additionnel"/></td>
                    <td>
                </tr>
                <tr>
                    <td><a href="#" class="aui-button aui-button-primary">fichier 3</a></td>
                    <td><input type="checkbox" name="contenu3" value="URL de contenu additionnel"/></td>
                    <td>
                </tr>
            </tbody>
        </table>
        <input type="submit" value="Supprimer" style="padding: 10px; display: block;float: right;margin-top: 25px;">
        </form>
				</fieldset>
			</div>
		</div>
	</div>
	
	
	
	<script type="text/javascript">
		$seanceOptiond = $('.seanceOptions');
		$caOptiond = $('.caOptions');
		$caOptiond.remove();
		$('input[id^="c"]').on('click', function(){
			if($(this).attr('id') == 'c2'){
				$seanceOptiond.remove();
				$('.contenuOptions').after($caOptiond);
			}else if($(this).attr('id') == 'c1'){
					$caOptiond.remove();
					$('.contenuOptions').after($seanceOptiond);
				}	
		});
	</script>
	
	 
	
	<%@include file="footer.jsp" %>