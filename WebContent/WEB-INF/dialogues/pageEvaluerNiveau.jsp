<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

		<%@ page import="java.util.ArrayList"%>
		<%@ page import="com.projectl.beans.Etudiant"%>
	
	<script type='text/javascript' src='/ProjetLicence/tipsy/javascripts/jquery.tipsy.js'></script>
	<link rel="stylesheet" href="/ProjetLicence/tipsy/stylesheets/tipsy.css" type="text/css" />
		
	<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min.css" media="all">
	<script src="/ProjetLicence/aui/js/aui.min.js"></script>
	
<script type="text/javascript" src="/ProjetLicence/easy-pie-chart/jquery.easypiechart.min.js"></script>





<div class="formulaire paintedGradient col-8 defaultFont">
	<h1>Evaluer le niveau</h1>
	<div id="moyenneNotes" >
		<h3>Le moyenne de module</h3>
		<div id="moyenneModule" class="paintedGradient" style="color: #565656;">
			<div class="chart" data-percent="<%=request.getAttribute("plusDeDix")%>">
				<span class="percent"></span>
				<p>Étudiant ayant eu au moins 10 au test.</p>
			</div>
			<div class="chart" data-percent="<%=request.getAttribute("plusDeQuinze")%>" style="float: right;">
				<span class="percent"></span>
				<p>Étudiant ayant au moins 15 au test.</p>
			</div>
			<script type="text/javascript">
			
				$('.chart').easyPieChart({
					barColor : '#209B92',
					trackColor : '#DAE4F7',
					scaleColor : 'transparent',
					lineCap : 'butt',
					lineWidth : 16,
					size : 185,
					animate : 1500,
					onStep : function(from, to, percent) {
						$(this.el).find('.percent').text(percent);
					}
				});
				
			</script>
		</div>
		
<%ArrayList<Etudiant> listeEtudiants =(ArrayList<Etudiant>) request.getAttribute("listeEtudiants"); %>
		
		<h3>Les notes des étudiants pour ce module</h3>
		<div id="notesEtudiants" class="paintedGradient">
			<table class="aui">
				<thead>
					<tr>
						<th>Nom</th>
						<th>Prénom</th>
						<th>E-mail</th>
						<th id="per">Note</th>
					</tr>
				</thead>
				<tbody>
				<% for(Etudiant etud : listeEtudiants){%>
					<tr>
						<td><%=etud.getNom()%></td>
						<td><%=etud.getPrenom()%></td>
						<td><%=etud.getEmail() %></td>
						<td><%=etud.getNote() %></td>
					</tr>
				<%} %>
				</tbody>
			</table>
		</div>








	</div>
</div>

<%@include file="footer.jsp"%>