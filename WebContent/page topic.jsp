<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Project L</title>
	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="description" content="Sublime Stunning free HTML5/CSS3 website template"/>
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox-thumbs.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox-buttons.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/fancybox.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/animate.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/button.css">
	<link rel="stylesheet" type="text/css" href="/ProjetLicence/css/main.css">
	<!-- 	<link rel="stylesheet" type="text/css" href="http://normalize-css.googlecode.com/svn/trunk/normalize.css">
	 -->
	<script type="text/javascript" src="/ProjetLicence/js/jquery.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-buttons.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-media.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/fancybox-thumbs.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/wow.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/main.js"></script>
	<script type="text/javascript" src="/ProjetLicence/js/login.js"></script>
	
	
	
	
	
	<script type='text/javascript' src='/ProjetLicence/tipsy/javascripts/jquery.tipsy.js'></script>
	<link rel="stylesheet" href="/ProjetLicence/tipsy/stylesheets/tipsy.css" type="text/css" />
		
	<link rel="stylesheet" href="/ProjetLicence/aui/css/aui.min-origin.css" media="all">
	<script src="/ProjetLicence/aui/js/aui.min.js"></script>
    
</head>
<body>

  <header id="header" role="banner">
    <!-- App Header goes inside #header -->

      <nav class="aui-header aui-dropdown2-trigger-group" role="navigation" data-aui-responsive="true">
          <div class="aui-header-primary">
              <h1 id="logo" class="aui-header-logo aui-header-logo-aui"><a href="http://example.com/"><span class="aui-header-logo-device">AUI</span></a></h1>
              <ul class="aui-nav" resolved="" style="width: auto;">
                  <li><a href="#" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="dropdown2-jira3" aria-expanded="false" resolved="">Menu</a>
                    <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header aui-layer" id="dropdown2-jira3" aria-hidden="true" resolved="">
                        <div class="aui-dropdown2-section">
                            <ul>
                              <li><a href="http://example.com/">Item 1</a></li>
                              <li><a href="http://example.com/">Item 2</a></li>
                              <li><a href="http://example.com/">Item 4</a></li>
                              <li><a href="http://example.com/">Item 4</a></li>
                              <li><a href="http://example.com/">Item 5</a></li>
                            </ul>
                        </div>
                        <div class="aui-dropdown2-section">
                            <ul>
                              <li><a href="http://example.com/">Item 6</a></li>
                              <li><a href="http://example.com/">Item 7</a></li>
                            </ul>
                        </div>
                    </div>
                  </li>
                  <!-- You can also use a split button in this location, if more than one primary action is required. -->
                  <li style="display: none;"><a id="aui-responsive-header-dropdown-trigger-1" class="  aui-dropdown2-trigger" aria-owns="aui-responsive-header-dropdown-content-1" aria-controls="aui-responsive-header-dropdown-content-1" aria-haspopup="true" href="#" data-aui-trigger="" resolved="" aria-expanded="false">More<span class="icon aui-icon-dropdown"></span></a><div id="aui-responsive-header-dropdown-content-1" class="aui-dropdown2 aui-style-default aui-layer" aria-hidden="true" resolved=""><div class="aui-dropdown2-section"><ul id="aui-responsive-header-dropdown-list-1"></ul></div></div></li><li style="display: none;"><aui-dropdown id="aui-responsive-header-dropdown-1" label="More" resolved=""><a class="aui-dropdown2-trigger" href="#" id="aui-responsive-header-dropdown-1-trigger" aria-controls="aui-responsive-header-dropdown-1-dropdown" aria-owns="aui-responsive-header-dropdown-1-dropdown" aria-haspopup="true" aria-expanded="false" resolved="">More</a><div aria-hidden="true" class="aui-dropdown2 aui-style-default aui-layer" role="menu" id="aui-responsive-header-dropdown-1-dropdown" resolved=""><div role="application"><!----><ul id="aui-responsive-header-dropdown-list-1"></ul><!----></div></div></aui-dropdown></li><li><a class="aui-button aui-button-primary" href="http://example.com/">Action</a></li>
              </ul>
          </div>
          <div class="aui-header-secondary">
              <ul class="aui-nav __skate" resolved="">
                  <li>
                      <form action="/foo" method="post" class="aui-quicksearch">
                          <label for="quicksearchid" class="assistive">Search</label>
                          <input id="quicksearchid" class="search" type="text" placeholder="Search..." name="quicksearchname">
                      </form>
                  </li>
                  <li><a href="#" aria-owns="dropdown2-header7" aria-haspopup="true" class="aui-dropdown2-trigger-arrowless aui-dropdown2-trigger" aria-controls="dropdown2-header7" aria-expanded="false" resolved=""><span class="aui-icon aui-icon-small aui-iconfont-help">Help</span></a>
                    <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header aui-layer" id="dropdown2-header7" style="display: none; top: 40px; min-width: 160px; left: 1213px; " aria-hidden="true" resolved="">
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="http://example.com/" class="active">Online help</a></li>
                                <li><a href="http://example.com/">Keyboard shortcuts</a></li>
                                <li><a href="http://example.com/">About</a></li>
                            </ul>
                        </div>
                    </div>
                  </li>
                  <li><a href="#" aria-owns="dropdown2-header9" aria-haspopup="true" class="aui-dropdown2-trigger-arrowless aui-dropdown2-trigger" aria-controls="dropdown2-header9" aria-expanded="false" resolved="">
                          <div class="aui-avatar aui-avatar-small">
                              <div class="aui-avatar-inner">
                                  <img src="../images/avatar-16.png">
                              </div>
                          </div>
                      </a>
                    <div class="aui-dropdown2 aui-style-default aui-dropdown2-in-header aui-layer" id="dropdown2-header9" style="display: none; top: 40px; min-width: 160px; left: 1213px;" aria-hidden="true" resolved="">
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="http://example.com/" class="active">Profile</a></li>
                                <li><a href="http://example.com/">Extensions</a></li>
                            </ul>
                        </div>
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="http://example.com/">Use Agile by default</a></li>
                            </ul>
                        </div>
                        <div class="aui-dropdown2-section">
                            <ul>
                                <li><a href="http://example.com/">Log out</a></li>
                            </ul>
                        </div>
                    </div>
                  </li>
              </ul>
          </div>
      </nav>

    <!-- App Header goes inside #header -->
    </header>
